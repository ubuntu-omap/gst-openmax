/*
 * Copyright (C) 2006-2009 Texas Instruments, Incorporated
 * Copyright (C) 2007-2009 Nokia Corporation.
 *
 * Author: Felipe Contreras <felipe.contreras@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <string.h>
#include <inttypes.h>
#include <unistd.h>

#include "gstomx_util.h"
#include "gstomx_port.h"
#include "gstomx.h"

#ifdef USE_OMXTICORE
#  include <OMX_TI_Common.h>
#  include <OMX_TI_Index.h>
#endif

#include <omap/omap_drm.h>
#include <libdrm/omap_drmif.h>

GST_DEBUG_CATEGORY_EXTERN (gstomx_util_debug);

#ifndef OMX_BUFFERFLAG_CODECCONFIG
#  define OMX_BUFFERFLAG_CODECCONFIG 0x00000080 /* special nFlags field to use to indicated codec-data */
#endif

#define OMX_BUFFERFLAG_INVALIDTIME 0x00000800 /* hack to pass buffers with invalid timestamps */

static OMX_BUFFERHEADERTYPE * request_buffer (GOmxPort *port);
static void release_buffer (GOmxPort *port, OMX_BUFFERHEADERTYPE *omx_buffer);
static void setup_shared_buffer (GOmxPort *port, OMX_BUFFERHEADERTYPE *omx_buffer);

#define DEBUG(port, fmt, args...) \
    GST_DEBUG ("<%s:%s> "fmt, GST_OBJECT_NAME ((port)->core->object), (port)->name, ##args)
#define LOG(port, fmt, args...) \
    GST_LOG ("<%s:%s> "fmt, GST_OBJECT_NAME ((port)->core->object), (port)->name, ##args)
#define WARNING(port, fmt, args...) \
    GST_WARNING ("<%s:%s> "fmt, GST_OBJECT_NAME ((port)->core->object), (port)->name, ##args)

/* When using dmabuf buffers instead of malloc(3)ed memory, this is
   the structure contained in the actual buffer data. There is currently
   only one fd allocated, but the omx camera test code can use up to 4,
   if there are multi plane buffers, so we do the same here in case we
   want to do that later. We also keep a ref to the device, as it is
   needed to recover the omap_bo struct from the fd when freeing it.
   The OMX code will expect only the fds, not the omap_device, which
   is here for us to be able to call omap_bo API. */
typedef struct {
    int32_t fds[4];
    struct omap_device *device;
} dmabufset;

/*
 * Port
 */

GOmxPort *
g_omx_port_new (GOmxCore *core, const gchar *name, guint index)
{
    GOmxPort *port = g_new0 (GOmxPort, 1);

    port->core = core;
    port->name = g_strdup_printf ("%s:%d", name, index);
    port->port_index = index;
    port->num_buffers = 0;
    port->buffers = NULL;

    port->enabled = TRUE;
    port->queue = async_queue_new ();
    port->mutex = g_mutex_new ();
    port->n_offset = 0;

    port->buffers_in_omx = 0;
    port->buffers_in_omx_mutex = g_mutex_new ();
    port->buffers_in_omx_cond = g_cond_new ();

    return port;
}

void
g_omx_port_free (GOmxPort *port)
{
    DEBUG (port, "begin");

    g_mutex_free (port->buffers_in_omx_mutex);
    g_cond_free (port->buffers_in_omx_cond);

    g_mutex_free (port->mutex);
    async_queue_free (port->queue);

    g_free (port->name);

    g_free (port->buffers);
    g_free (port);

    GST_DEBUG ("end");
}

void
g_omx_port_setup (GOmxPort *port,
                  OMX_PARAM_PORTDEFINITIONTYPE *omx_port)
{
    GOmxPortType type = -1;

    switch (omx_port->eDir)
    {
        case OMX_DirInput:
            type = GOMX_PORT_INPUT;
            break;
        case OMX_DirOutput:
            type = GOMX_PORT_OUTPUT;
            break;
        default:
            break;
    }

    port->type = type;
    /** @todo should it be nBufferCountMin? */
    port->num_buffers = omx_port->nBufferCountActual;
    port->port_index = omx_port->nPortIndex;

    DEBUG (port, "type=%d, num_buffers=%d, port_index=%d",
        port->type, port->num_buffers, port->port_index);

    /* I don't think it is valid for buffers to be allocated at this point..
     * if there is a case where it is, then call g_omx_port_free_buffers()
     * here instead:
     */
    g_return_if_fail (!port->buffers);
}

/* Frees a dmabuf based buffer */
static void
free_dmabuf (void *ptr)
{
    dmabufset *fds = ptr;
    int n;

    GST_DEBUG ("Freeing dmabufset %p", ptr);
    for (n = 0; n < 4; n++) {
        if (fds->fds[n]) {
            close (fds->fds[n]);
        }
    }
    omap_device_del (fds->device);
    free (fds);
}

/* Allocate a dmabuf based buffer */
static void *
alloc_dmabuf (gint len, GstDRMBufferPool *pool)
{
    struct omap_bo *bo;
    dmabufset *fds;

    /* The following will currently leak, but is unused.
       We cannot use a buffer that's not been signaled before
       using OMX_UseBuffer, but the new buffer here may be
       any size, so we cannot reliably reuse another buffer.
       Still, for camera, this will never be called (this is
       used for codec headers), so we do not care for now. */
    GST_DEBUG ("Allocating %d buffer from pool %p", len, pool);
    bo = omap_bo_new (pool->dev, len, OMAP_BO_WC);
    if (!bo)
        return NULL;
    fds = malloc (sizeof (dmabufset));
    fds->fds[0] = omap_bo_dmabuf (bo);
    fds->fds[1] = fds->fds[2] = fds->fds[3] = 0;
    fds->device = omap_device_ref (pool->dev);

    return fds;
}

static void *
make_dmabufset (GstBuffer *buffer)
{
    GstDRMBuffer *drm_buffer = GST_DRM_BUFFER (buffer);
    struct omap_bo *bo = drm_buffer->bo;
    dmabufset *fds;

    fds = malloc (sizeof (dmabufset));
    fds->fds[0] = omap_bo_dmabuf (bo);
    fds->fds[1] = fds->fds[2] = fds->fds[3] = 0;
    fds->device = omap_device_ref (drm_buffer->pool->dev);
    GST_DEBUG ("Wrapping fd %d, dmabufset %p, buffer %"GST_PTR_FORMAT,
        fds->fds[0], fds, buffer);

    return fds;
}

static void
set_dmabuf (OMX_BUFFERHEADERTYPE *omx_buffer, GstBuffer *buffer)
{
    GstDRMBuffer *drm_buffer = GST_DRM_BUFFER (buffer);
    struct omap_bo *bo = drm_buffer->bo;
    dmabufset *fds = (dmabufset *)omx_buffer->pBuffer;

    if (fds->fds[0])
        close (fds->fds[0]);
    fds->fds[0] = omap_bo_dmabuf (bo);
    GST_DEBUG ("Setting fd %d in dmabufset %p from bo %p to buffer %"GST_PTR_FORMAT,
        fds->fds[0], fds, bo, buffer);
}

static GstBuffer *
buffer_alloc (GOmxPort *port, gint len)
{
    GstBuffer *buf = NULL;

    DEBUG (port, "buffer_alloc %d, port fn %p", len, port->buffer_alloc);
    if (port->pool) {
        buf = gst_drm_buffer_pool_get (port->pool, FALSE);
        if (buf) {
            DEBUG (port, "Allocated pool buffer");
        } else {
            WARNING (port, "FAILED to allocate pool buffer");
        }
    } else {
        WARNING (port, "No pool to allocate from");
    }

    return buf;
}


/**
 * Ensure that srcpad caps are set before beginning transition-to-idle or
 * transition-to-loaded.  This is a bit ugly, because it requires pad-alloc'ing
 * a buffer from the downstream element for no particular purpose other than
 * triggering upstream caps negotiation from the sink..
 */
gboolean
g_omx_port_prepare (GOmxPort *port)
{
    OMX_PARAM_PORTDEFINITIONTYPE param;
    OMX_CONFIG_RECTTYPE frame;
    OMX_ERRORTYPE omxError;
    GstBuffer *buf;
    guint size;

    DEBUG (port, "begin");

    G_OMX_PORT_GET_DEFINITION (port, &param);
    size = param.nBufferSize;
    switch (param.eDomain) {
      case OMX_PortDomainVideo:
        port->frame_width = param.format.video.nFrameWidth;
        port->frame_height = param.format.video.nFrameHeight;
        break;
    default:
        port->frame_width = -1;
        port->frame_height = -1;
        break;
    }

    _G_OMX_INIT_PARAM (&frame);
    frame.nPortIndex = port->port_index;
    omxError = OMX_GetParameter(port->core->omx_handle,
        OMX_TI_IndexParam2DBufferAllocDimension,
        &frame);
    if (omxError != OMX_ErrorNone) {
      DEBUG (port, "2D buffer allocation: %d %d", frame.nWidth, frame.nHeight);
      port->allocated_height = frame.nHeight;
    } else {
      WARNING (port, "Error getting 2D buffer allocation: %x", omxError);
      port->allocated_height = -1;
    }

    buf = buffer_alloc (port, size);
    if (!buf) {
        WARNING (port, "Failed to allocate buffer");
        return FALSE;
    }

    if (GST_BUFFER_SIZE (buf) != size)
    {
        DEBUG (port, "buffer sized changed, %d->%d",
                size, GST_BUFFER_SIZE (buf));
    }

    /* number of buffers could have changed */
    G_OMX_PORT_GET_DEFINITION (port, &param);
    port->num_buffers = param.nBufferCountActual;
    DEBUG (port, "num-buffers=%d", port->num_buffers);

    gst_buffer_unref (buf);

#ifdef USE_OMXTICORE
    {
        OMX_TI_PARAM_BUFFERPREANNOUNCE param;
        OMX_TI_CONFIG_BUFFERREFCOUNTNOTIFYTYPE config;

        G_OMX_PORT_GET_PARAM (port, OMX_TI_IndexParamBufferPreAnnouncement, &param);
        param.bEnabled = FALSE;
        G_OMX_PORT_SET_PARAM (port, OMX_TI_IndexParamBufferPreAnnouncement, &param);

        G_OMX_PORT_GET_CONFIG (port, OMX_TI_IndexConfigBufferRefCountNotification, &config);
        config.bNotifyOnDecrease = TRUE;
        config.bNotifyOnIncrease = FALSE;
        config.nCountForNotification = 1;
        G_OMX_PORT_SET_CONFIG (port, OMX_TI_IndexConfigBufferRefCountNotification, &config);
    }
#endif

    DEBUG (port, "end");

    return TRUE;
}

/* Pushes freed buffers back onto OMX */
static void
g_omx_port_buffer_pooled (GstDRMBuffer *buffer, GOmxPort *port)
{
    guint i;

    g_return_if_fail (GST_IS_DRM_BUFFER (buffer));

    DEBUG (port, "Buffer %p pooled, enabled %d, submit %d",
        buffer, port->enabled, port->submit_pooled_buffers);
    if (!port->enabled || !port->submit_pooled_buffers)
       return;

    for (i = 0; i < port->num_buffers; i++)
       if (port->buffers[i]->pAppPrivate == buffer)
           break;

    if (i < port->num_buffers) {
        DEBUG (port, "Got free buffer %p from pool", buffer);
        setup_shared_buffer (port, port->buffers[i]);
        release_buffer (port, port->buffers[i]);
    } else {
         WARNING (port, "Could not find associated OMX buffer for %p", buffer);
    }
}

void
g_omx_port_allocate_buffers (GOmxPort *port)
{
    OMX_PARAM_PORTDEFINITIONTYPE param;
    guint i;
    guint size;
    OMX_ERRORTYPE err;
    OMX_TI_PARAM_USEBUFFERDESCRIPTOR desc;
    GstBuffer *buffer;

    if (port->buffers)
        return;

    DEBUG (port, "begin");

    /* Configure the port to use DMA buffers */
    _G_OMX_INIT_PARAM (&desc);
    desc.nPortIndex = port->port_index;
    desc.bEnabled = OMX_FALSE; /* 1D buffer */
    G_OMX_CORE_SET_PARAM(port->core, (OMX_INDEXTYPE) OMX_TI_IndexUseDmaBuffers, &desc);

    G_OMX_PORT_GET_DEFINITION (port, &param);
    size = param.nBufferSize;

    port->buffers = g_new0 (OMX_BUFFERHEADERTYPE *, port->num_buffers);

    for (i = 0; i < port->num_buffers; i++)
    {

        {
            gpointer buffer_data = NULL;

            /* All the buffers we allocate here will be pinned,
               and will be the only ones that we can use later.
               So we need to cycle through all the ones that are
               allocated below, and thus need to keep track of
               each of the fds to which they correspond. */
            buffer = gst_drm_buffer_pool_get (port->pool, FALSE);
            buffer_data = make_dmabufset (buffer);
            DEBUG (port, "dmabuf fd is %d, fd array %p", ((int*)buffer_data)[0], buffer_data);

            /* See comments about free ordering in g_omx_port_free_buffers */
            GST_BUFFER_MALLOCDATA (buffer) = buffer_data;
            GST_BUFFER_FREE_FUNC (buffer) = (GFreeFunc) free_dmabuf;

            err = OMX_UseBuffer (port->core->omx_handle,
                           &port->buffers[i],
                           port->port_index,
                           NULL,
                           size,
                           buffer_data);
            if (err != OMX_ErrorNone) {
                WARNING (port, "OMX_UseBuffer error: %x", err);
            }

            g_return_if_fail (port->buffers[i]);
            DEBUG (port, "OMX_UseBuffer: OMX buffer %d: %p, fd %d, pBuffer %p, gst buffer %p",
                i, port->buffers[i], ((int*)buffer_data)[0], buffer_data, buffer);

            port->buffers[i]->pAppPrivate = buffer;

            /* we will need this later: */
            port->buffers[i]->nAllocLen = size;
        }
    }

    /* Once all buffers are allocated and pinned, we can unref the gst
       buffers, which will be returned to the pool, and register for
       their pooled event. Since the port is not yet enabled, this
       unref will not yet cause an automatic submit to OMX. */
    for (i = 0; i < port->num_buffers; i++) {
      GstBuffer *buffer = port->buffers[i]->pAppPrivate;
      g_signal_connect (buffer, "pooled", G_CALLBACK (g_omx_port_buffer_pooled), port);
      gst_buffer_unref (buffer);
    }

    port->buffers_in_omx = 0;

    DEBUG (port, "end");
}

void
g_omx_port_free_buffers (GOmxPort *port)
{
    guint i;
    OMX_BUFFERHEADERTYPE *omx_buffer;

    if (!port->buffers)
        return;

    DEBUG (port, "begin");

    /* g_omx_port_free_buffers can be called from elsehwere than stop,
       so enabled might not be FALSE when buffers get re-pooled, so we
       maintain our own boolean here to know when to stop resubmitting */
    port->submit_pooled_buffers = FALSE;

    /* The order of some deletion steps is important here:
     *  - OMX_FreeBuffer may access the underlying fds in pBuffer
     *  - fds must be kept till the gst buffer is finalized
     *  - buffers may still be in the pipeline when we run the following code
     * So fds are freed in a custom free_buffer function.
     *
     * If buffers are in the port queue, then they hold a ref. If not, then
     * they logically have no ref, but the pool will have a ref to them to
     * keep them alive in the pool. Thus, we need to unref those buffers
     * that are in the port queue only. Since we set submit_pooled_buffers
     * to FALSE earlier, they will not be pushed back into OMX and will
     * be put back in the pool, where the only ref to them will be held by
     * the pool, which will be destroyed below. That is when the final free
     * code (which frees DRM fds) will happen.
     */

    g_mutex_lock (port->buffers_in_omx_mutex);
    while (port->buffers_in_omx > 0) {
        DEBUG (port, "At least one buffer is still in OMX, waiting");
        g_cond_wait (port->buffers_in_omx_cond, port->buffers_in_omx_mutex);
    }
    g_mutex_unlock (port->buffers_in_omx_mutex);

    DEBUG (port, "Flushing %u buffers", port->queue->length);
    while (port->queue->length > 0) {
      OMX_BUFFERHEADERTYPE *omx_buffer = async_queue_pop_full (port->queue, TRUE, TRUE);
      GstBuffer *buffer = GST_BUFFER (omx_buffer->pAppPrivate);
      gst_buffer_unref (buffer);
    }

    for (i = 0; i < port->num_buffers; i++) {
        omx_buffer = port->buffers[i];
        DEBUG (port, "OMX_FreeBuffer(%p, %p)", omx_buffer, omx_buffer->pBuffer);
        g_signal_handlers_disconnect_by_func (omx_buffer->pAppPrivate, G_CALLBACK (g_omx_port_buffer_pooled), port);
        omx_buffer->pAppPrivate = NULL;
        OMX_FreeBuffer (port->core->omx_handle, port->port_index, omx_buffer);
        port->buffers[i] = NULL;
    }

    g_free (port->buffers);
    port->buffers = NULL;

    if (port->pool) {
        DEBUG (port, "Destroying buffer pool");
        gst_drm_buffer_pool_destroy (port->pool);
        port->pool = NULL;
    }

    DEBUG (port, "end");
}

void
g_omx_port_start_buffers (GOmxPort *port)
{
    guint i;

    if (!port->enabled)
        return;

    g_return_if_fail (port->buffers);

    port->submit_pooled_buffers = TRUE;

    DEBUG (port, "begin, port type %s, %d buffers",
        (port->type == GOMX_PORT_INPUT) ? "input" : "output", port->num_buffers);

    for (i = 0; i < port->num_buffers; i++)
    {
        OMX_BUFFERHEADERTYPE *omx_buffer;

        omx_buffer = port->buffers[i];

        /* If it's an input port we will need to fill the buffer, so put it in
         * the queue, otherwise send to omx for processing (fill it up). */
        if (port->type == GOMX_PORT_INPUT)
        {
            g_omx_core_got_buffer (port->core, port, omx_buffer);
        }
        else
        {
            setup_shared_buffer (port, omx_buffer);
            release_buffer (port, omx_buffer);
        }
    }

    DEBUG (port, "end");
}

void
g_omx_port_push_buffer (GOmxPort *port,
                        OMX_BUFFERHEADERTYPE *omx_buffer)
{
    g_mutex_lock (port->buffers_in_omx_mutex);
    if (!--port->buffers_in_omx) {
        g_cond_signal (port->buffers_in_omx_cond);
    }
    g_mutex_unlock (port->buffers_in_omx_mutex);

    /* If we've started flushing the queue, do not push into it */
    if (!port->submit_pooled_buffers) {
        DEBUG (port, "Ignoring buffer, we're either inactive, or flushing");
        gst_buffer_unref (omx_buffer->pAppPrivate);
        return;
    }

    DEBUG (port, "Pushing buffer %p", omx_buffer);
    async_queue_push (port->queue, omx_buffer);
}

static OMX_BUFFERHEADERTYPE *
request_buffer (GOmxPort *port)
{
    OMX_BUFFERHEADERTYPE *buffer;
    LOG (port, "request buffer");
    while (1) {
      buffer = async_queue_pop (port->queue);
      if (!buffer)
        break;
      if (!port->drop_late_buffers)
        break;
      if (async_queue_is_empty (port->queue))
        break;
      if (buffer->pAppPrivate)
        gst_buffer_unref (buffer->pAppPrivate);
    }
    LOG (port, "requested buffer %p", buffer);
    return buffer;
}

static void
release_buffer (GOmxPort *port, OMX_BUFFERHEADERTYPE *omx_buffer)
{
    OMX_ERRORTYPE error = OMX_ErrorNone;

    g_mutex_lock (port->buffers_in_omx_mutex);
    port->buffers_in_omx++;
    g_mutex_unlock (port->buffers_in_omx_mutex);

    switch (port->type)
    {
        case GOMX_PORT_INPUT:
            DEBUG (port, "ETB: omx_buffer=%p, pAppPrivate=%p, pBuffer=%p",
                    omx_buffer, omx_buffer ? omx_buffer->pAppPrivate : 0, omx_buffer ? omx_buffer->pBuffer : 0);
            error = OMX_EmptyThisBuffer (port->core->omx_handle, omx_buffer);
            break;
        case GOMX_PORT_OUTPUT:
            DEBUG (port, "FTB: omx_buffer=%p, pAppPrivate=%p, pBuffer=%p (fd %d)",
                    omx_buffer, omx_buffer ? omx_buffer->pAppPrivate : 0, omx_buffer ? omx_buffer->pBuffer : 0,
                    omx_buffer && omx_buffer->pBuffer ? *(int*)(omx_buffer->pBuffer): -1);
            error = OMX_FillThisBuffer (port->core->omx_handle, omx_buffer);
            break;
        default:
            break;
    }

    if (error != OMX_ErrorNone) {
         GST_ERROR ("release_buffer: OMX error: %x", error);
    }
}

/* NOTE ABOUT BUFFER SHARING:
 *
 * Buffer sharing is a sort of "extension" to OMX to allow zero copy buffer
 * passing between GST and OMX.
 *
 * There are only two cases:
 *
 * 1) shared_buffer is enabled, in which case we control nOffset, and use
 *    pAppPrivate to store the reference to the original GstBuffer that
 *    pBuffer ptr is copied from.  Note that in case of input buffers,
 *    the DSP/coprocessor should treat the buffer as read-only so cache-
 *    line alignment is not an issue.  For output buffers which are not
 *    pad_alloc()d, some care may need to be taken to ensure proper buffer
 *    alignment.
 * 2) shared_buffer is not enabled, in which case we respect the nOffset
 *    set by the component and pAppPrivate is NULL
 *
 */

static void
setup_shared_buffer (GOmxPort *port, OMX_BUFFERHEADERTYPE *omx_buffer)
{
    GstBuffer *new_buf = buffer_alloc (port, omx_buffer->nAllocLen);

    omx_buffer->pAppPrivate = new_buf;
    set_dmabuf (omx_buffer, new_buf);
    omx_buffer->nOffset     = 0;
    omx_buffer->nFlags      = 0;

    /* special hack.. this should be removed: */
    omx_buffer->nFlags     |= OMX_BUFFERHEADERFLAG_MODIFIED;
}

typedef void (*SendPrep) (GOmxPort *port, OMX_BUFFERHEADERTYPE *omx_buffer, gpointer obj);

static void
memcpy_to_omx_buffer (OMX_BUFFERHEADERTYPE *omx_buffer, const guint8 *data)
{
  dmabufset *d;
  struct omap_bo *bo;
  guint8 *omx_ptr;

  d = (dmabufset*)omx_buffer->pBuffer;
  bo = omap_bo_from_dmabuf (d->device, d->fds[0]);
  omx_ptr = omap_bo_map (bo);

  GST_DEBUG ("memcpy_to_omx_buffer: omx_buffer %p, dev %p, fd %d, mapped bo %p, data %p",
      omx_buffer, d->device, d->fds[0], omx_ptr, data);
  memcpy (omx_ptr + omx_buffer->nOffset, data, omx_buffer->nFilledLen);

  omap_bo_del (bo);
}

static void
memcpy_from_omx_buffer (OMX_BUFFERHEADERTYPE *omx_buffer, guint8 *data)
{
  dmabufset *d;
  struct omap_bo *bo;
  guint8 *omx_ptr;

  d = (dmabufset*)omx_buffer->pBuffer;
  bo = omap_bo_from_dmabuf (d->device, d->fds[0]);
  omx_ptr = omap_bo_map (bo);

  GST_DEBUG ("memcpy_from_omx_buffer: omx_buffer %p, dev %p, fd %d, mapped bo %p, data %p",
      omx_buffer, d->device, d->fds[0], omx_ptr, data);
  memcpy (data, omx_ptr + omx_buffer->nOffset, omx_buffer->nFilledLen);

  omap_bo_del (bo);
}

static void
send_prep_codec_data (GOmxPort *port, OMX_BUFFERHEADERTYPE *omx_buffer, GstBuffer *buf)
{
    omx_buffer->nFlags |= OMX_BUFFERFLAG_CODECCONFIG;
    omx_buffer->nFilledLen = GST_BUFFER_SIZE (buf);

    omx_buffer->nOffset = 0;
    omx_buffer->pBuffer = alloc_dmabuf (omx_buffer->nFilledLen, port->pool);
    DEBUG (port, "Untested code path at %u\n", __LINE__);

    memcpy_to_omx_buffer (omx_buffer, GST_BUFFER_DATA (buf));
}

static void
send_prep_buffer_data (GOmxPort *port, OMX_BUFFERHEADERTYPE *omx_buffer, GstBuffer *buf)
{
    DEBUG (port, "Untested code path at %u", __LINE__);

    omx_buffer->nOffset     = port->n_offset;
    set_dmabuf (omx_buffer, buf);
    omx_buffer->nFilledLen  = GST_BUFFER_SIZE (buf);
    /* Temp hack to not update nAllocLen for each ETB/FTB till we
     * find a cleaner solution to get padded width and height */
    /* omx_buffer->nAllocLen   = GST_BUFFER_SIZE (buf); */
    omx_buffer->pAppPrivate = gst_buffer_ref (buf);

    /* special hack.. this should be removed: */
    omx_buffer->nFlags     |= OMX_BUFFERHEADERFLAG_MODIFIED;

    if (port->core->use_timestamps)
    {
        if (GST_CLOCK_TIME_IS_VALID (GST_BUFFER_TIMESTAMP (buf)))
        {
            omx_buffer->nTimeStamp = gst_util_uint64_scale_int (
                    GST_BUFFER_TIMESTAMP (buf),
                    OMX_TICKS_PER_SECOND, GST_SECOND);
        }
        else
        {
            omx_buffer->nFlags |= OMX_BUFFERFLAG_INVALIDTIME;
        }
    }

    DEBUG (port, "omx_buffer: size=%lu, len=%lu, flags=%lu, offset=%lu, timestamp=%lld",
            omx_buffer->nAllocLen, omx_buffer->nFilledLen, omx_buffer->nFlags,
            omx_buffer->nOffset, omx_buffer->nTimeStamp);
}

static void
send_prep_eos_event (GOmxPort *port, OMX_BUFFERHEADERTYPE *omx_buffer, GstEvent *evt)
{
    omx_buffer->nFlags |= OMX_BUFFERFLAG_EOS;
    omx_buffer->nFilledLen = 0;

    /* OMX should not try to read from the buffer, since it is empty..
     * but yet it complains if pBuffer is NULL.  This will get us past
     * that check, and ensure that OMX segfaults in a debuggible way
     * if they do something stupid like read from the empty buffer:
     */
    omx_buffer->pBuffer    = (OMX_U8 *)1;
    /* TODO: Temporary hack as OMX currently complains about
     * non-zero nAllocLen. Need to be removed once aligned with OMX.
     */
    /* omx_buffer->nAllocLen  = 0; */
}

/**
 * Send a buffer/event to the OMX component.  This handles conversion of
 * GST buffer, codec-data, and EOS events to the equivalent OMX buffer.
 *
 * This method does not take ownership of the ref to @obj
 *
 * Returns number of bytes sent, or negative if error
 */
gint
g_omx_port_send (GOmxPort *port, gpointer obj)
{
    SendPrep send_prep = NULL;

    g_return_val_if_fail (port->type == GOMX_PORT_INPUT, -1);

    if (GST_IS_BUFFER (obj))
    {
        if (G_UNLIKELY (GST_BUFFER_FLAG_IS_SET (obj, GST_BUFFER_FLAG_IN_CAPS)))
            send_prep = (SendPrep)send_prep_codec_data;
        else
            send_prep = (SendPrep)send_prep_buffer_data;
    }
    else if (GST_IS_EVENT (obj))
    {
        if (G_LIKELY (GST_EVENT_TYPE (obj) == GST_EVENT_EOS))
            send_prep = (SendPrep)send_prep_eos_event;
    }

    if (G_LIKELY (send_prep))
    {
        gint ret;
        OMX_BUFFERHEADERTYPE *omx_buffer = request_buffer (port);

        if (!omx_buffer)
        {
            DEBUG (port, "null buffer");
            return -1;
        }

        /* don't assume OMX component clears flags!
         */
        omx_buffer->nFlags = 0;

        /* if buffer sharing is enabled, pAppPrivate might hold the ref to
         * a buffer that is no longer required and should be unref'd.  We
         * do this check here, rather than in send_prep_buffer_data() so
         * we don't keep the reference live in case, for example, this time
         * the buffer is used for an EOS event.
         */
        if (omx_buffer->pAppPrivate)
        {
            GstBuffer *old_buf = omx_buffer->pAppPrivate;
            gst_buffer_unref (old_buf);
            omx_buffer->pAppPrivate = NULL;
            omx_buffer->pBuffer = NULL;     /* just to ease debugging */
        }

        send_prep (port, omx_buffer, obj);

        ret = omx_buffer->nFilledLen;

        release_buffer (port, omx_buffer);

        return ret;
    }

    WARNING (port, "unknown obj type");
    return -1;
}

static void
rearrange_buffer (GOmxPort * port, GstBuffer * buf)
{
    if (port->allocated_height > 0) {
        /* For some reason, the chroma layout isn't what I'd expect
         * when the allocated and requested sizes are different:
         * Both luma and chroma planes have a stride consistent with
         * the requested width and height, but the chroma plane are
         * placed at an offset consistent with a combination of the
         * requested width and the allocated height.
         * So we need to move the chroma plane here to match what is
         * expected.
         * These offsets were obtained by experimentation with a few
         * different sizes, so I'm not sure whether the formula used
         * below is fully correct, but it works with the cases I've
         * seen. These cases were all for stereo video capture.
         * The formula assumes NV12, which is always the case for
         * stereo capture. Since I've no idea if this gap can happen
         * for non stereo capture, and, if it does, whether it would
         * follow the same offset calculation, I see no need to make
         * it color format generic as there's no evidence it would be
         * correct in the first place.
         * Note that stridetransform is unable to do this as it does
         * not know of such a weird setup.
         */
        if (port->frame_height != port->allocated_height) {
            DEBUG (port, "Allocated height different from requested, fixing up chroma plane");
            memmove ( GST_BUFFER_DATA (buf) + port->frame_width * port->frame_height,
              GST_BUFFER_DATA (buf) + port->frame_width * port->allocated_height,
              port->frame_width * port->frame_height / 2
            );
        }
    }
}

/**
 * Receive a buffer/event from OMX component.  This handles the conversion
 * of OMX buffer to GST buffer, codec-data, or EOS event.
 *
 * Returns <code>NULL</code> if buffer could not be received.
 */
gpointer
g_omx_port_recv (GOmxPort *port)
{
    gpointer ret = NULL;

    g_return_val_if_fail (port->type == GOMX_PORT_OUTPUT, NULL);

    while (!ret && port->enabled)
    {
        OMX_BUFFERHEADERTYPE *omx_buffer = request_buffer (port);

        if (G_UNLIKELY (!omx_buffer))
        {
            return NULL;
        }

        DEBUG (port, "omx_buffer=%p size=%lu, len=%lu, flags=%lu, offset=%lu, timestamp=%lld",
                omx_buffer, omx_buffer->nAllocLen, omx_buffer->nFilledLen, omx_buffer->nFlags,
                omx_buffer->nOffset, omx_buffer->nTimeStamp);

        if (G_UNLIKELY (omx_buffer->nFlags & OMX_BUFFERFLAG_EOS))
        {
            DEBUG (port, "got eos");
            gst_buffer_replace ((GstBuffer **)&omx_buffer->pAppPrivate, NULL);
            ret = gst_event_new_eos ();
        }
        else if (G_LIKELY (omx_buffer->nFilledLen > 0))
        {
            GstBuffer *buf = omx_buffer->pAppPrivate;

            /* I'm not really sure if it was intentional to block zero-copy of
             * the codec-data buffer.. this is how the original code worked,
             * so I kept the behavior
             */
            if (!buf || (omx_buffer->nFlags & OMX_BUFFERFLAG_CODECCONFIG))
            {
                if (buf)
                {
                    gst_buffer_unref (buf);
                    omx_buffer->pAppPrivate = NULL;
                }

                buf = buffer_alloc (port, omx_buffer->nFilledLen);
                memcpy_from_omx_buffer (omx_buffer, GST_BUFFER_DATA (buf));
            }
            else if (buf)
            {
                /* don't rely on OMX having told us the correct buffer size
                 * when we allocated the buffer.
                 */
                GST_BUFFER_SIZE (buf) = omx_buffer->nFilledLen;
                DEBUG (port, "Received %u bytes from OMX", GST_BUFFER_SIZE (buf));
                rearrange_buffer (port, buf);
            }

            if (port->core->use_timestamps)
            {
                if (omx_buffer->nFlags & OMX_BUFFERFLAG_INVALIDTIME)
                {
                    GST_BUFFER_TIMESTAMP (buf) = GST_CLOCK_TIME_NONE;
                }
                else
                {
                    GST_BUFFER_TIMESTAMP (buf) = gst_util_uint64_scale_int (
                            omx_buffer->nTimeStamp,
                            GST_SECOND, OMX_TICKS_PER_SECOND);
                }
            }

            if (G_UNLIKELY (omx_buffer->nFlags & OMX_BUFFERFLAG_CODECCONFIG))
            {
                GST_BUFFER_FLAG_SET (buf, GST_BUFFER_FLAG_IN_CAPS);
            }

            port->n_offset = omx_buffer->nOffset;

            ret = buf;
        }
        else
        {
            GstBuffer *buf = omx_buffer->pAppPrivate;

            if (buf)
            {
                gst_buffer_unref (buf);
                omx_buffer->pAppPrivate = NULL;
            }

            DEBUG (port, "empty buffer %p", omx_buffer); /* keep looping */
        }

#ifdef USE_OMXTICORE
        if (omx_buffer->nFlags & OMX_TI_BUFFERFLAG_READONLY)
        {
            GstBuffer *buf = omx_buffer->pAppPrivate;

            if (buf)
            {
                /* if using buffer sharing, create an extra ref to the buffer
                 * to account for the fact that the OMX component is still
                 * holding a reference.  (This prevents the buffer from being
                 * free'd while the component is still using it as, for ex, a
                 * reference frame.)
                 */
                gst_buffer_ref (buf);
            }

            DEBUG (port, "dup'd buffer %p", omx_buffer);

            g_mutex_lock (port->core->omx_state_mutex);
            omx_buffer->nFlags &= ~OMX_TI_BUFFERFLAG_READONLY;
            g_mutex_unlock (port->core->omx_state_mutex);
        }
        else if (omx_buffer->nFlags & GST_BUFFERFLAG_UNREF_CHECK)
        {
            /* buffer has already been handled under READONLY case.. so
             * don't return it to gst.  Just unref it, and release the omx
             * buffer which was previously not released.
             */
            gst_buffer_unref(ret);
            ret = NULL;

            DEBUG (port, "unref'd buffer %p", omx_buffer);

            setup_shared_buffer (port, omx_buffer);
            release_buffer (port, omx_buffer);
        }
        else
#endif
        {
            /* Here, we do not send the buffer back to OMX anymore,
               as the corresponding gst buffer is still in the pipeline.
               When the gst buffer is unreffed enough to go back to the
               pool, we will get notified and will only then setup the
               buffer to send it back to OMX. */
        }
    }


    return ret;
}

void
g_omx_port_resume (GOmxPort *port)
{
    DEBUG (port, "resume");
    async_queue_enable (port->queue);
}

void
g_omx_port_pause (GOmxPort *port)
{
    DEBUG (port, "pause");
    async_queue_disable (port->queue);
}

void
g_omx_port_flush (GOmxPort *port)
{
    DEBUG (port, "begin");

    DEBUG (port, "SendCommand(Flush, %d)", port->port_index);
    OMX_SendCommand (port->core->omx_handle, OMX_CommandFlush, port->port_index, NULL);
    g_sem_down (port->core->flush_sem);

    if (port->type == GOMX_PORT_OUTPUT)
    {
        /* This will get rid of any buffers that we have received, but not
         * yet processed in the output_loop.
         */
        OMX_BUFFERHEADERTYPE *omx_buffer;
        while ((omx_buffer = async_queue_pop_full (port->queue, FALSE, TRUE)))
        {
            omx_buffer->nFilledLen = 0;

#ifdef USE_OMXTICORE
            if (omx_buffer->nFlags & OMX_TI_BUFFERFLAG_READONLY)
            {
                /* For output buffer that is marked with READONLY, we
                   cannot release until EventHandler OMX_TI_EventBufferRefCount
                   come. So, reset the nFlags to be released later. */
                DEBUG (port, "During flush encounter ReadOnly buffer %p", omx_buffer);
                g_mutex_lock (port->core->omx_state_mutex);
                omx_buffer->nFlags &= ~OMX_TI_BUFFERFLAG_READONLY;
                g_mutex_unlock (port->core->omx_state_mutex);
            }
            else
#endif
            {
                release_buffer (port, omx_buffer);
            }
        }
    }
    DEBUG (port, "end");
}

gboolean
g_omx_port_enable (GOmxPort *port)
{
    if (port->enabled)
    {
        DEBUG (port, "already enabled");
        return TRUE;
    }

    DEBUG (port, "begin");

    if (!g_omx_port_prepare (port)) {
        DEBUG (port, "g_omx_port_prepare failed");
        return FALSE;
    }

    DEBUG (port, "SendCommand(PortEnable, %d)", port->port_index);
    OMX_SendCommand (g_omx_core_get_handle (port->core),
            OMX_CommandPortEnable, port->port_index, NULL);

    g_omx_port_allocate_buffers (port);

    g_sem_down (port->core->port_sem);

    port->enabled = TRUE;

    if (port->core->omx_state == OMX_StateExecuting)
        g_omx_port_start_buffers (port);

    DEBUG (port, "end");

    return TRUE;
}

void
g_omx_port_disable (GOmxPort *port)
{
    if (!port->enabled)
    {
        DEBUG (port, "already disabled");
        return;
    }

    DEBUG (port, "begin");

    port->enabled = FALSE;

    DEBUG (port, "SendCommand(PortDisable, %d)", port->port_index);
    OMX_SendCommand (g_omx_core_get_handle (port->core),
            OMX_CommandPortDisable, port->port_index, NULL);

    g_omx_port_free_buffers (port);

    g_sem_down (port->core->port_sem);

    DEBUG (port, "end");
}

void
g_omx_port_finish (GOmxPort *port)
{
    DEBUG (port, "finish");
    port->enabled = FALSE;
    async_queue_disable (port->queue);
}


/*
 * Some domain specific port related utility functions:
 */

/* keep this list in sync GSTOMX_ALL_FORMATS */
static gint32 all_fourcc[] = {
        GST_MAKE_FOURCC ('N','V','1','2'),
        GST_MAKE_FOURCC ('I','4','2','0'),
        GST_MAKE_FOURCC ('Y','U','Y','2'),
        GST_MAKE_FOURCC ('U','Y','V','Y'),
};

#ifndef DIM  /* XXX is there a better alternative available? */
#  define DIM(x) (sizeof(x)/sizeof((x)[0]))
#endif

/**
 * A utility function to query the port for supported color formats, and
 * add the appropriate list of formats to @caps.  The @port can either
 * be an input port for a video encoder, or an output port for a decoder
 */
GstCaps *
g_omx_port_set_video_formats (GOmxPort *port, GstCaps *caps)
{
    OMX_VIDEO_PARAM_PORTFORMATTYPE param;
    int i,j;

    G_OMX_PORT_GET_PARAM (port, OMX_IndexParamVideoPortFormat, &param);

    caps = gst_caps_make_writable (caps);

    for (i=0; i<gst_caps_get_size (caps); i++)
    {
        GstStructure *struc = gst_caps_get_structure (caps, i);
        GValue formats = {0};

        g_value_init (&formats, GST_TYPE_LIST);

        for (j=0; j<DIM(all_fourcc); j++)
        {
            OMX_ERRORTYPE err;
            GValue fourccval = {0};

            g_value_init (&fourccval, GST_TYPE_FOURCC);

            /* check and see if OMX supports the format:
             */
            param.eColorFormat = g_omx_fourcc_to_colorformat (all_fourcc[j]);
            err = G_OMX_PORT_SET_PARAM (port, OMX_IndexParamVideoPortFormat, &param);

            if( err == OMX_ErrorIncorrectStateOperation )
            {
                DEBUG (port, "already executing?");

                /* if we are already executing, such as might be the case if
                 * we get a OMX_EventPortSettingsChanged event, just take the
                 * current format and bail:
                 */
                G_OMX_PORT_GET_PARAM (port, OMX_IndexParamVideoPortFormat, &param);
                gst_value_set_fourcc (&fourccval,
                        g_omx_colorformat_to_fourcc (param.eColorFormat));
                gst_value_list_append_value (&formats, &fourccval);
                break;
            }
            else if( err == OMX_ErrorNone )
            {
                gst_value_set_fourcc (&fourccval, all_fourcc[j]);
                gst_value_list_append_value (&formats, &fourccval);
            }
        }

        gst_structure_set_value (struc, "format", &formats);
    }

    return caps;
}

    /*For avoid repeated code needs to do only one function in order to configure
    video and images caps strure, and also maybe adding RGB color format*/

static gint32 jpeg_fourcc[] = {
        GST_MAKE_FOURCC ('U','Y','V','Y'),
        GST_MAKE_FOURCC ('N','V','1','2')
};

/**
 * A utility function to query the port for supported color formats, and
 * add the appropriate list of formats to @caps.  The @port can either
 * be an input port for a image encoder, or an output port for a decoder
 */
GstCaps *
g_omx_port_set_image_formats (GOmxPort *port, GstCaps *caps)
{
    //OMX_IMAGE_PARAM_PORTFORMATTYPE param;
    int i,j;

    //G_OMX_PORT_GET_PARAM (port, OMX_IndexParamImagePortFormat, &param);

    caps = gst_caps_make_writable (caps);

    for (i=0; i<gst_caps_get_size (caps); i++)
    {
        GstStructure *struc = gst_caps_get_structure (caps, i);
        GValue formats = {0};

        g_value_init (&formats, GST_TYPE_LIST);

        for (j=0; j<DIM(jpeg_fourcc); j++)
        {
            //OMX_ERRORTYPE err;
            GValue fourccval = {0};

            g_value_init (&fourccval, GST_TYPE_FOURCC);

        /* Got error from omx jpeg component , avoiding these lines by the moment till they support it*/
#if 0
            /* check and see if OMX supports the format:
             */
            param.eColorFormat = g_omx_fourcc_to_colorformat (all_fourcc[j]);
            err = G_OMX_PORT_SET_PARAM (port, OMX_IndexParamImagePortFormat, &param);

            if( err == OMX_ErrorIncorrectStateOperation )
            {
                DEBUG (port, "already executing?");

                /* if we are already executing, such as might be the case if
                 * we get a OMX_EventPortSettingsChanged event, just take the
                 * current format and bail:
                 */
                G_OMX_PORT_GET_PARAM (port, OMX_IndexParamImagePortFormat, &param);
                gst_value_set_fourcc (&fourccval,
                        g_omx_colorformat_to_fourcc (param.eColorFormat));
                gst_value_list_append_value (&formats, &fourccval);
                break;
            }
            else if( err == OMX_ErrorNone )
            {
                gst_value_set_fourcc (&fourccval, all_fourcc[j]);
                gst_value_list_append_value (&formats, &fourccval);
            }
#else
            gst_value_set_fourcc (&fourccval, jpeg_fourcc[j]);
            gst_value_list_append_value (&formats, &fourccval);
#endif
        }

        gst_structure_set_value (struc, "format", &formats);
    }

    return caps;
}

void
g_omx_port_set_buffer_pool (GOmxPort *port, GstDRMBufferPool *pool)
{
    if (port->pool) {
        WARNING (port, "Already have a pool");
    }

    if (pool)
        gst_mini_object_ref (GST_MINI_OBJECT (pool));
    if (port->pool) {
        gst_drm_buffer_pool_destroy (port->pool);
    }
    port->pool = pool;
}
