/* GStreamer
 *
 * Copyright (C) 2009 Texas Instruments, Inc - http://www.ti.com/
 * Copyright (C) 2011 Collabora Ltda
 *  @author: Luciana Fujii Pontello <luciana.fujii@collabora.co.uk>
 *
 * Description: OMX Camera element
 *  Created on: Aug 31, 2009
 *      Author: Rob Clark <rob@ti.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "config.h"

#include "gstomx_camera_parameters.h"
#include "gstomx_camera.h"
#include "gstomx.h"
#include "timer-32k.h"

#include <dce.h>

#include <gst/video/video.h>
#include <gst/interfaces/photography.h>
#include <gst/interfaces/photography-enumtypes.h>
#include <gst/interfaces/colorbalance.h>

#ifdef USE_OMXTICORE
#  include <OMX_TI_IVCommon.h>
#  include <OMX_TI_Index.h>
#  include <OMX_TI_Common.h>
#endif

#ifdef HAVE_X11
#  include <X11/Xlib.h>
#  include <X11/Xmd.h>
#  include <X11/extensions/dri2proto.h>
#  include <X11/extensions/dri2.h>
#endif

#include <stdint.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <OMX_CoreExt.h>
#include <OMX_IndexExt.h>

/**
 * SECTION:element-omx_camerasrc
 *
 * omx_camerasrc can be used to capture video and/or still frames from OMX
 * camera.  It can also be used as a filter to provide access to the camera's
 * memory-to-memory mode.
 * <p>
 * In total, the omx_camerasrc element exposes one optional input port, "sink",
 * one mandatory src pad, "src", and two optional src pads, "imgsrc" and
 * "vidsrc".  If "imgsrc" and/or "vidsrc" are linked, then viewfinder buffers
 * are pushed on the "src" pad.
 * <p>
 * In all modes, preview buffers are pushed on the "src" pad.  In video capture
 * mode, the same buffer is pushed on the "vidsrc" pad.  In image capture mode,
 * a separate full resolution image (either raw or jpg encoded) is pushed on
 * the "imgsrc" pad.
 * <p>
 * The camera pad_alloc()s buffers from the "src" pad, in order to allocate
 * memory from the video driver.  The "vidsrc" caps are slaved to the "src"
 * caps.  Although this should be considered an implementation detail.
 * <p>
 * TODO: for legacy mode support, as a replacement for v4l2src, can we push
 * buffers of the requested resolution on the "src" pad?  Can we configure the
 * OMX component for arbitrary resolution on the preview port, or do we need
 * to dynamically map the "src" pad to different ports depending on the config?
 * The OMX camera supports only video resolutions on the preview and video
 * ports, but supports higher resolution stills on the image port.
 *
 * <refsect2>
 * <title>Example launch lines</title>
 * |[
 * gst-launch omx_camera vstab=1 mode=2 vnf=1 name=cam cam.src ! queue ! v4l2sink \
 * cam.vidsrc ! "video/x-raw-yuv, format=(fourcc)UYVY, width=720, height=480, framerate=30/1" ! \
 * queue ! omx_h264enc matroskamux name=mux ! filesink location=capture.mkv ! \
 * alsasrc ! "audio/x-raw-int,rate=48000,channels=1, width=16, depth=16, endianness=1234" ! \
 * queue ! omx_aacenc bitrate=64000 profile=2 ! "audio/mpeg,mpegversion=4,rate=48000,channels=1" ! \
 * mux. cam.imgsrc ! "image/jpeg, width=720, height=480" ! filesink name=capture.jpg
 * ]|
 * </refsect2>
 */

static void create_ports (GstOmxCamera *self);
static GstClockTime get_latency (GstOmxCamera * self);

static const GstElementDetails element_details =
GST_ELEMENT_DETAILS ("Video OMX Camera Source",
    "Source/Video",
    "Reads frames from a OMX Camera Component",
    "Rob Clark <rob@ti.com>");

static gboolean
gst_omx_camera_interface_supported (GstImplementsInterface * iface,
    GType type)
{
    g_assert (type == GST_TYPE_PHOTOGRAPHY || type == GST_TYPE_COLOR_BALANCE);
    return TRUE;
}

static void gst_omx_camera_photography_init (GstPhotographyInterface *iface);
static void gst_omx_camera_colorbalance_init (GstColorBalanceClass *iface);

static void
gst_omx_camera_interface_init (GstImplementsInterfaceClass * klass)
{
    klass->supported = gst_omx_camera_interface_supported;
}


static void
_do_init (GType omx_camera_type)
{
    static const GInterfaceInfo iface_info = {
        (GInterfaceInitFunc) gst_omx_camera_interface_init,
        NULL,
        NULL,
    };
    static const GInterfaceInfo photography_info = {
        (GInterfaceInitFunc) gst_omx_camera_photography_init,
        NULL,
        NULL,
    };

    static const GInterfaceInfo colorbalance_info = {
      (GInterfaceInitFunc) gst_omx_camera_colorbalance_init,
      NULL,
      NULL,
    };

    g_type_add_interface_static (omx_camera_type,
            GST_TYPE_IMPLEMENTS_INTERFACE, &iface_info);
    g_type_add_interface_static (omx_camera_type, GST_TYPE_PHOTOGRAPHY,
            &photography_info);
    g_type_add_interface_static (omx_camera_type, GST_TYPE_COLOR_BALANCE,
            &colorbalance_info);
}

GSTOMX_BOILERPLATE_FULL (GstOmxCamera, gst_omx_camera, GstOmxBaseSrc,
        GST_OMX_BASE_SRC_TYPE, _do_init);

const GList *
gst_omx_camera_colorbalance_list_channels (GstColorBalance *balance)
{
    GstOmxCamera *self = GST_OMX_CAMERA (balance);

    g_return_val_if_fail (self != NULL, NULL);
    return self->channels;
}

static gint
gst_omx_camera_colorbalance_get_value (GstColorBalance *balance,
                                       GstColorBalanceChannel *channel)
{
    GstOmxCamera *self = GST_OMX_CAMERA (balance);
    gint value = 0;

    g_return_val_if_fail (self != NULL, 0);

    if (!g_ascii_strcasecmp (channel->label, "BRIGHTNESS"))
        gst_omx_camera_get_brightness (self, &value);
    else if (!g_ascii_strcasecmp (channel->label, "SATURATION"))
        gst_omx_camera_get_saturation (self, &value);
    else if (!g_ascii_strcasecmp (channel->label, "SHARPNESS"))
        gst_omx_camera_get_sharpness (self, &value);
    else if (!g_ascii_strcasecmp (channel->label, "CONTRAST"))
        gst_omx_camera_get_contrast (self, &value);
    return value;
}

static void
gst_omx_camera_colorbalance_set_value (GstColorBalance *balance,
                                       GstColorBalanceChannel *channel,
                                       gint value)
{
    GstOmxCamera *self = GST_OMX_CAMERA (balance);

    g_return_if_fail (self != NULL);

    if (!g_ascii_strcasecmp (channel->label, "BRIGHTNESS"))
        gst_omx_camera_set_brightness (self, value);
    else if (!g_ascii_strcasecmp (channel->label, "SATURATION"))
        gst_omx_camera_set_saturation (self, value);
    else if (!g_ascii_strcasecmp (channel->label, "SHARPNESS"))
        gst_omx_camera_set_sharpness (self, value);
    else if (!g_ascii_strcasecmp (channel->label, "CONTRAST"))
        gst_omx_camera_set_contrast (self, value);
}

static void gst_omx_camera_colorbalance_init (GstColorBalanceClass *iface)
{
  iface->list_channels = gst_omx_camera_colorbalance_list_channels;
  iface->get_value = gst_omx_camera_colorbalance_get_value;
  iface->set_value = gst_omx_camera_colorbalance_set_value;
}

static void gst_omx_camera_photography_init (GstPhotographyInterface *iface)
{
  iface->get_ev_compensation = gst_omx_camera_photography_get_ev_compensation;
  iface->get_iso_speed = gst_omx_camera_photography_get_iso_speed;
  iface->get_white_balance_mode = gst_omx_camera_photography_get_white_balance_mode;
  iface->get_scene_mode = gst_omx_camera_photography_get_scene_mode;
  iface->get_zoom = gst_omx_camera_photography_get_zoom;
  iface->get_flicker_mode = gst_omx_camera_photography_get_flicker_mode;

  iface->set_ev_compensation = gst_omx_camera_photography_set_ev_compensation;
  iface->set_iso_speed = gst_omx_camera_photography_set_iso_speed;
  iface->set_white_balance_mode = gst_omx_camera_photography_set_white_balance_mode;
  iface->set_scene_mode = gst_omx_camera_photography_set_scene_mode;
  iface->set_zoom = gst_omx_camera_photography_set_zoom;
  iface->set_flicker_mode = gst_omx_camera_photography_set_flicker_mode;
  iface->set_autofocus = gst_omx_camera_photography_set_autofocus;
  iface->get_capabilities = gst_omx_camera_photography_get_capabilities;
}


#define USE_GSTOMXCAM_IMGSRCPAD
//#define USE_GSTOMXCAM_VIDSRCPAD
//#define USE_GSTOMXCAM_THUMBSRCPAD
//#define USE_GSTOMXCAM_IN_PORT


/*
 * Caps:
 */

/* Experimentation shows that OMX only accepts NV12 and UYVY
   colorspaces, and not I420 nor YUY2. In stereo mode, only
   NV12 is allowed. */
#undef GSTOMX_ALL_FORMATS
#define GSTOMX_ALL_FORMATS  "{NV12, UYVY}"

static GstStaticPadTemplate src_template = GST_STATIC_PAD_TEMPLATE ("src",
        GST_PAD_SRC,
        GST_PAD_ALWAYS,
        GST_STATIC_CAPS (
                "video/x-raw-rgb, bpp=16, depth=16, red_mask=63488, "
                "green_mask=2016, blue_mask=31, endianness=1234, "
                "width=(int)[1,max], height=(int)[1,max], "
                "framerate=(fraction)[0,max]; "
                GST_VIDEO_CAPS_YUV (GSTOMX_ALL_FORMATS))
    );

#ifdef USE_GSTOMXCAM_IMGSRCPAD
static GstStaticPadTemplate imgsrc_template = GST_STATIC_PAD_TEMPLATE ("imgsrc",
        GST_PAD_SRC,
        GST_PAD_ALWAYS,
        /* Note: imgsrc pad supports JPEG format, Bayer, as well as
           non-strided YUV. */
        GST_STATIC_CAPS (
                "image/jpeg, width=(int)[1,max], height=(int)[1,max]; "
                "video/x-raw-bayer, width=(int)[1,max], height=(int)[1,max]; "
                GST_VIDEO_CAPS_YUV (GSTOMX_ALL_FORMATS))
    );
#endif

#ifdef USE_GSTOMXCAM_VIDSRCPAD
static GstStaticPadTemplate vidsrc_template = GST_STATIC_PAD_TEMPLATE ("vidsrc",
        GST_PAD_SRC,
        GST_PAD_ALWAYS,
        GST_STATIC_CAPS (
                "video/x-raw-rgb, bpp=16, depth=16, red_mask=63488, "
                "green_mask=2016, blue_mask=31, endianness=1234, "
                "width=(int)[1,max], height=(int)[1,max], "
                "framerate=(fraction)[0,max]; "
                GST_VIDEO_CAPS_YUV (GSTOMX_ALL_FORMATS))
    );
#endif

#ifdef USE_GSTOMXCAM_THUMBSRCPAD
static GstStaticPadTemplate thumbsrc_template = GST_STATIC_PAD_TEMPLATE ("thumbsrc",
        GST_PAD_SRC,
        GST_PAD_REQUEST,
        GST_STATIC_CAPS (
                "video/x-raw-bayer, width=(int)[1,max], height=(int)[1,max]; "
                GST_VIDEO_CAPS_RGB "; "
                GST_VIDEO_CAPS_RGB_16 "; "
                GST_VIDEO_CAPS_YUV (GSTOMX_ALL_FORMATS))
    );
#endif

#ifdef USE_GSTOMXCAM_IN_PORT
static GstStaticPadTemplate sink_template = GST_STATIC_PAD_TEMPLATE ("sink",
        GST_PAD_SINK,
        GST_PAD_ALWAYS,
        GST_STATIC_CAPS ("???")
    );
#endif

static void
setup_pools (GstOmxCamera *self)
{
    GstOmxBaseSrc *omx_base = GST_OMX_BASE_SRC (self);
    OMX_PARAM_PORTDEFINITIONTYPE param;
    GOmxPort *port, *ports[] = { omx_base->out_port, self->img_port };
    GstPad *pads[] = { GST_BASE_SRC_PAD (omx_base), self->imgsrcpad };
    gint i;
    GstDRMBufferPool *buffer_pool;

    for (i = 0; i < G_N_ELEMENTS (ports); i++) {
      port = ports[i];
      G_OMX_PORT_GET_DEFINITION (port, &param);
      buffer_pool = gst_drm_buffer_pool_new (GST_ELEMENT (self), self->drm_fd,
          GST_PAD_CAPS (pads[i]), param.nBufferSize);
      g_omx_port_set_buffer_pool (port, buffer_pool);
    }
}

static gboolean
src_setcaps (GstPad *pad, GstCaps *caps)
{
    GstOmxCamera *self = GST_OMX_CAMERA (GST_PAD_PARENT (pad));
    GstOmxBaseSrc *omx_base = GST_OMX_BASE_SRC (self);

    GstVideoFormat format;
    gint width, height, rowstride = 0;
    guint32 xFramerate;
    OMX_ERRORTYPE err;
    OMX_CONFIG_RECTTYPE frame;
    guint size;

    GST_INFO_OBJECT (omx_base, "setcaps (src/vidsrc): %" GST_PTR_FORMAT, caps);

    if (gst_video_format_parse_caps_strided (caps,
            &format, &width, &height, &rowstride))
    {
        /* Output port configuration: */
        OMX_PARAM_PORTDEFINITIONTYPE param;
        gboolean configure_port = FALSE;

        G_OMX_PORT_GET_DEFINITION (omx_base->out_port, &param);

        if (rowstride == 0)
          rowstride = gst_video_format_get_row_stride (format, 0, width);

        if ((param.format.video.nFrameWidth != width) ||
           (param.format.video.nFrameHeight != height) ||
           (param.format.video.nStride != rowstride))
        {
            param.format.video.nFrameWidth  = width;
            param.format.video.nFrameHeight = height;
            param.format.video.nStride      = self->rowstride = rowstride;
            configure_port = TRUE;
        }

        param.nBufferSize = gst_video_format_get_size (format, width, height);
        GST_DEBUG ("Alloc buffer size for format %d, %d x %d: %u", format, width, height, param.nBufferSize);

        /* special hack to work around OMX camera bug:
         */
        if (param.format.video.eColorFormat != g_omx_gstvformat_to_colorformat (format))
        {
            if (g_omx_gstvformat_to_colorformat (format) == OMX_COLOR_FormatYUV420PackedSemiPlanar)
            {
                if (param.format.video.eColorFormat != OMX_COLOR_FormatYUV420SemiPlanar)
                {
                    param.format.video.eColorFormat = OMX_COLOR_FormatYUV420SemiPlanar;
                    configure_port = TRUE;
                }
            }
            else
            {
                param.format.video.eColorFormat = g_omx_gstvformat_to_colorformat (format);
                configure_port = TRUE;
            }
        }

        gst_structure_get_fraction (gst_caps_get_structure (caps, 0),
            "framerate", &self->fps_n, &self->fps_d);


        xFramerate = (self->fps_n << 16) / self->fps_d;

        if (param.format.video.xFramerate != xFramerate)
        {
            param.format.video.xFramerate = xFramerate;
            configure_port = TRUE;
        }

        /* At the moment we are only using preview port and not vid_port
         * From omx camera desing document we are missing
         * SetParam CommonSensormode -> bOneShot = FALSE ?
         */

        if (configure_port)
        {
            gboolean port_enabled = FALSE;

            if (omx_base->out_port->enabled && (omx_base->gomx->omx_state != OMX_StateLoaded))
            {
                g_omx_port_disable (omx_base->out_port);
                port_enabled = TRUE;
            }

            err = G_OMX_PORT_SET_DEFINITION (omx_base->out_port, &param);
            if (err != OMX_ErrorNone) {
                GST_ERROR_OBJECT (omx_base, "Failed to set definition: %x", err);
                return FALSE;
            }

            if (port_enabled) {
                if (!g_omx_port_enable (omx_base->out_port)) {
                    GST_ERROR_OBJECT (omx_base, "Failed to enable port");
                    return FALSE;
                }
            }
        }

        GST_INFO_OBJECT (omx_base, " Rowstride=%d, Width=%d, Height=%d, Color=%d, Buffersize=%d, framerate=%.1f",
            param.format.video.nStride, param.format.video.nFrameWidth, param.format.video.nFrameHeight, param.format.video.eColorFormat, param.nBufferSize,param.format.video.xFramerate/65536.0f );

    /* Ask OMX for the allocated size it wants. It may be different
       that the size we want to use, which would mean we need to
       update our buffer size to be large enough. */
    _G_OMX_INIT_PARAM (&frame);
    frame.nPortIndex = omx_base->out_port->port_index;
    err = OMX_GetParameter(omx_base->gomx->omx_handle,
        OMX_TI_IndexParam2DBufferAllocDimension, &frame);
    if (err != OMX_ErrorNone) {
        GST_WARNING_OBJECT (omx_base, "Error getting 2D buffer allocation: %x", err);
    } else {
        GST_INFO_OBJECT (omx_base, "2D buffer allocation: %d %d", frame.nWidth, frame.nHeight);
        size = gst_video_format_get_size (format, frame.nWidth, frame.nHeight);
        if (size != param.nBufferSize) {
            GST_DEBUG_OBJECT (omx_base,
                "2D allocated size different from requested size (%u -> %u), resetting port definition",
                param.nBufferSize, size);
            param.nBufferSize = size;
            err = G_OMX_PORT_SET_DEFINITION (omx_base->out_port, &param);
            if (err != OMX_ErrorNone) {
                GST_ERROR_OBJECT (omx_base, "Failed to set definition: %x", err);
                return FALSE;
            }
        }
    }

#ifdef USE_OMXTICORE
        self->img_regioncenter_x = (param.format.video.nFrameWidth / 2);
        self->img_regioncenter_y = (param.format.video.nFrameHeight / 2);
#endif

        if  (!gst_pad_set_caps (GST_BASE_SRC (self)->srcpad, caps))
            return FALSE;

        GST_INFO_OBJECT (omx_base, " exit setcaps src: %");
    }

    return TRUE;
}

static void
src_fixatecaps (GstPad *pad, GstCaps *caps)
{
    GstOmxCamera *self = GST_OMX_CAMERA (GST_PAD_PARENT (pad));
    GstStructure *structure;
    int scale;

    structure = gst_caps_get_structure (caps, 0);

    scale = (self->device == OMX_TI_StereoSensor) ? 2 : 1;
    gst_structure_fixate_field_nearest_int (structure, "width", 864 * scale);
    gst_structure_fixate_field_nearest_int (structure, "height", 480);
    gst_structure_fixate_field_nearest_fraction (structure, "framerate", 30, 1);
}

#ifdef USE_GSTOMXCAM_IMGSRCPAD
static gboolean
imgsrc_setcaps (GstPad *pad, GstCaps *caps)
{
    GstOmxCamera *self = GST_OMX_CAMERA (GST_PAD_PARENT (pad));
    GstOmxBaseSrc *omx_base = GST_OMX_BASE_SRC (self);

    GstVideoFormat format;
    gint width, height, rowstride;
    GstStructure *s;

    GST_INFO_OBJECT (omx_base, "setcaps (imgsrc): %" GST_PTR_FORMAT, caps);

    g_return_val_if_fail (caps, FALSE);
    g_return_val_if_fail (gst_caps_is_fixed (caps), FALSE);

    if (gst_structure_has_name (s=gst_caps_get_structure (caps, 0), "image/jpeg"))
    {
        /* Output port configuration for JPEG: */
        OMX_PARAM_PORTDEFINITIONTYPE param;

        GST_DEBUG_OBJECT (self, "set JPEG format");

        G_OMX_PORT_GET_DEFINITION (self->img_port, &param);

        gst_structure_get_int (s, "width", &width);
        gst_structure_get_int (s, "height", &height);

        param.format.image.eColorFormat = OMX_COLOR_FormatCbYCrY;
        param.format.image.eCompressionFormat = OMX_IMAGE_CodingJPEG;
        param.format.image.nFrameWidth  = width;
        param.format.image.nFrameHeight = height;
        param.format.image.nStride      = 0;

        GST_INFO_OBJECT (self, "Rowstride=%d, Width=%d, Height=%d, Buffersize=%d, num-buffer=%d",
            param.format.image.nStride, param.format.image.nFrameWidth, param.format.image.nFrameHeight, param.nBufferSize, param.nBufferCountActual);

        G_OMX_PORT_SET_DEFINITION (self->img_port, &param);
    }
    else if (gst_structure_has_name (s=gst_caps_get_structure (caps, 0),
                     "video/x-raw-bayer"))
    {
        /* Output port configuration for Bayer: */
        OMX_PARAM_PORTDEFINITIONTYPE param;

        GST_DEBUG_OBJECT (self, "set Raw-Bayer format");

        G_OMX_PORT_GET_DEFINITION (self->img_port, &param);

        gst_structure_get_int (s, "width", &width);
        gst_structure_get_int (s, "height", &height);

        param.format.image.eColorFormat = OMX_COLOR_FormatRawBayer10bit;
        param.format.image.eCompressionFormat = OMX_IMAGE_CodingUnused;
        param.format.image.nFrameWidth  = width;
        param.format.image.nFrameHeight = height;
        param.format.image.nStride      = width * 2;

        GST_INFO_OBJECT (self, "Rowstride=%d, Width=%d, Height=%d, "
            "Buffersize=%d, num-buffer=%d", param.format.image.nStride,
            param.format.image.nFrameWidth, param.format.image.nFrameHeight,
            param.nBufferSize, param.nBufferCountActual);

        G_OMX_PORT_SET_DEFINITION (self->img_port, &param);
        G_OMX_PORT_GET_DEFINITION (self->img_port, &param);
    }
    else if (gst_video_format_parse_caps_strided (caps,
            &format, &width, &height, &rowstride))
    {
        /* Output port configuration for YUV: */
        OMX_PARAM_PORTDEFINITIONTYPE param;

        GST_DEBUG_OBJECT (self, "set raw format");

        G_OMX_PORT_GET_DEFINITION (self->img_port, &param);

        param.format.image.eCompressionFormat = OMX_IMAGE_CodingUnused;
        param.format.image.eColorFormat = g_omx_gstvformat_to_colorformat (format);
        param.format.image.nFrameWidth  = width;
        param.format.image.nFrameHeight = height;
        param.format.image.nStride      = rowstride;

        /* special hack to work around OMX camera bug:
         */
        if (param.format.video.eColorFormat == OMX_COLOR_FormatYUV420PackedSemiPlanar)
            param.format.video.eColorFormat = OMX_COLOR_FormatYUV420SemiPlanar;

        G_OMX_PORT_SET_DEFINITION (self->img_port, &param);
    }

    return TRUE;
}

static void
imgsrc_fixatecaps (GstPad *pad, GstCaps *caps)
{
    GstStructure *structure;

    structure = gst_caps_get_structure (caps, 0);

    gst_structure_fixate_field_nearest_int (structure, "width", 864);
    gst_structure_fixate_field_nearest_int (structure, "height", 480);
}

static gboolean
imgsrc_event (GstPad * pad, GstEvent * event)
{
    GstOmxCamera *self = GST_OMX_CAMERA (gst_pad_get_parent (pad));
    gboolean res = FALSE;

    switch (GST_EVENT_TYPE (event)) {
      case GST_EVENT_CUSTOM_BOTH:
      {
        const GstStructure *s;
        GstCaps *caps;

        s = gst_event_get_structure (event);
        if (strcmp (gst_structure_get_name (s), "renegotiate"))
          break;

        caps = gst_pad_peer_get_caps (pad);
        if (!gst_caps_is_fixed (caps)) {
          caps = gst_caps_make_writable (caps);
          gst_pad_fixate_caps (pad, caps);
        }
        GST_INFO_OBJECT (self, "renegotiating image caps %"GST_PTR_FORMAT, caps);
        res = gst_pad_set_caps (pad, caps);
        GST_INFO_OBJECT (self, "negotiation result: %d", res);
        gst_caps_unref (caps);

        break;
      }
      default:
        break;
    }

    gst_object_unref (self);
    return res;
}
#endif

#ifdef USE_GSTOMXCAM_THUMBSRCPAD
static gboolean
thumbsrc_setcaps (GstPad *pad, GstCaps *caps)
{
    GstOmxCamera *self = GST_OMX_CAMERA (GST_PAD_PARENT (pad));
    GstOmxBaseSrc *omx_base = GST_OMX_BASE_SRC (self);

    GstVideoFormat format;
    gint width, height;
    GstStructure *s;

    GST_INFO_OBJECT (omx_base, "setcaps (thumbsrc): %" GST_PTR_FORMAT, caps);

    g_return_val_if_fail (caps, FALSE);
    g_return_val_if_fail (gst_caps_is_fixed (caps), FALSE);

    if (gst_video_format_parse_caps (caps, &format, &width, &height))
    {
        /* Output port configuration for RAW: */
        OMX_PARAM_PORTDEFINITIONTYPE param;

        GST_DEBUG_OBJECT (self, "set YUV/RGB raw format");

        G_OMX_PORT_GET_DEFINITION (self->vid_port, &param);

        param.format.image.eCompressionFormat = OMX_VIDEO_CodingUnused;
        param.format.image.eColorFormat = g_omx_gstvformat_to_colorformat (format);
        param.format.image.nFrameWidth  = width;
        param.format.image.nFrameHeight = height;

        /* special hack to work around OMX camera bug:
         */
        if (param.format.video.eColorFormat == OMX_COLOR_FormatYUV420PackedSemiPlanar)
            param.format.video.eColorFormat = OMX_COLOR_FormatYUV420SemiPlanar;

        G_OMX_PORT_SET_DEFINITION (self->vid_port, &param);
    }
    else if (gst_structure_has_name (s=gst_caps_get_structure (caps, 0),
                     "video/x-raw-bayer"))
    {
        /* Output port configuration for Bayer: */
        OMX_PARAM_PORTDEFINITIONTYPE param;

        GST_DEBUG_OBJECT (self, "set Raw-Bayer format");

        G_OMX_PORT_GET_DEFINITION (self->vid_port, &param);

        gst_structure_get_int (s, "width", &width);
        gst_structure_get_int (s, "height", &height);

        param.format.image.eColorFormat = OMX_COLOR_FormatRawBayer10bit;
        param.format.image.eCompressionFormat = OMX_VIDEO_CodingUnused;
        param.format.image.nFrameWidth  = width;
        param.format.image.nFrameHeight = height;

        GST_INFO_OBJECT (self, "Width=%d, Height=%d, Buffersize=%d, num-buffer=%d",
            param.format.image.nFrameWidth, param.format.image.nFrameHeight,
            param.nBufferSize, param.nBufferCountActual);

        G_OMX_PORT_SET_DEFINITION (self->vid_port, &param);
    }

    return TRUE;
}
#endif

static GstClockTime
get_latency (GstOmxCamera * self)
{
    if (G_UNLIKELY (self->latency == GST_CLOCK_TIME_NONE)) {
      if (self->fps_d == 0) {
        GST_INFO_OBJECT (self, "not ready to report latency");
        return GST_CLOCK_TIME_NONE;
      }

      if (self->fps_n != 0)
        self->latency = gst_util_uint64_scale (GST_SECOND,
            4 * self->fps_d, self->fps_n);
      else
        self->latency = 0;

    }

    return self->latency;
}

static gboolean
src_query (GstPad *pad, GstQuery *query)
{
    GstOmxCamera *self = GST_OMX_CAMERA (GST_PAD_PARENT (pad));
    GstOmxBaseSrc *omx_base = GST_OMX_BASE_SRC (self);
    gboolean ret = FALSE;

    GST_DEBUG_OBJECT (self, "Begin");

    switch (GST_QUERY_TYPE (query))
    {
        case GST_QUERY_BUFFERS:
        {
            OMX_ERRORTYPE err;
            OMX_PARAM_PORTDEFINITIONTYPE param;

            _G_OMX_INIT_PARAM (&param);

            param.nPortIndex = omx_base->out_port->port_index;
            err = OMX_GetParameter (omx_base->gomx->omx_handle,
                    OMX_IndexParamPortDefinition, &param);
            g_assert (err == OMX_ErrorNone);

            GST_DEBUG_OBJECT (self, "Actual buffers: %d", param.nBufferCountActual);

            gst_query_set_buffers_count (query, param.nBufferCountActual);

#ifdef USE_OMXTICORE
            {
                OMX_CONFIG_RECTTYPE rect;
                _G_OMX_INIT_PARAM (&rect);

                rect.nPortIndex = omx_base->out_port->port_index;
                err = OMX_GetParameter (omx_base->gomx->omx_handle,
                        OMX_TI_IndexParam2DBufferAllocDimension, &rect);
                if (err == OMX_ErrorNone)
                {
                    GST_DEBUG_OBJECT (self, "Min dimensions: %dx%d",
                            rect.nWidth, rect.nHeight);

                    gst_query_set_buffers_dimensions (query,
                            rect.nWidth, rect.nHeight);
                }
            }
#endif

            ret = TRUE;
            break;
        }

        case GST_QUERY_LATENCY:
        {
          GstClockTime min, max, latency;

          gst_query_parse_latency (query, NULL, &min, &max);
          latency = get_latency (self);
          if (min == GST_CLOCK_TIME_NONE)
            min = latency;
          else
            min += latency;

          if (max != GST_CLOCK_TIME_NONE)
            max += latency;

          GST_INFO_OBJECT (self, "reporting latency %"GST_TIME_FORMAT,
              GST_TIME_ARGS (min));
          gst_query_set_latency (query, TRUE, min, max);

          ret = TRUE;
          break;
        }

        default:
            ret = GST_BASE_SRC_CLASS (parent_class)->query (GST_BASE_SRC (self), query);
    }

    GST_DEBUG_OBJECT (self, "End -> %d", ret);

    return ret;
}

/* note.. maybe this should be moved somewhere common... GstOmxBaseVideoDec has
 * almost same logic..
 */
static void
settings_changed (GstElement *self, GstPad *pad)
{
    GstCaps *new_caps;

    if (!gst_pad_is_linked (pad))
    {
        GST_DEBUG_OBJECT (self, "%"GST_PTR_FORMAT": pad is not linked", pad);
        return;
    }

    new_caps = gst_caps_intersect (gst_pad_get_caps (pad),
           gst_pad_peer_get_caps (pad));

    if (!gst_caps_is_fixed (new_caps))
    {
        gst_caps_do_simplify (new_caps);

        if (GST_PAD_CAPS (pad) && gst_caps_is_subset (GST_PAD_CAPS(pad), new_caps))
        {
            gst_caps_replace (&new_caps, GST_PAD_CAPS(pad));
        }

        GST_INFO_OBJECT (self, "%"GST_PTR_FORMAT": pre-fixated caps: %" GST_PTR_FORMAT, pad, new_caps);
        gst_pad_fixate_caps (pad, new_caps);
    }

    GST_INFO_OBJECT (self, "%"GST_PTR_FORMAT": caps are: %" GST_PTR_FORMAT, pad, new_caps);
    GST_INFO_OBJECT (self, "%"GST_PTR_FORMAT": old caps are: %" GST_PTR_FORMAT, pad, GST_PAD_CAPS (pad));

    gst_pad_set_caps (pad, new_caps);
    gst_caps_unref (new_caps);
}

static void
settings_changed_cb (GOmxCore *core)
{
    GstOmxCamera *self = core->object;

    GST_DEBUG_OBJECT (self, "settings changed");

    settings_changed (GST_ELEMENT (self), GST_BASE_SRC (self)->srcpad);

#ifdef USE_GSTOMXCAM_VIDSRCPAD
    settings_changed (GST_ELEMENT (self), self->vidsrcpad);
#endif
#ifdef USE_GSTOMXCAM_IMGSRCPAD
    settings_changed (GST_ELEMENT (self), self->imgsrcpad);
#endif
#ifdef USE_GSTOMXCAM_THUMBSRCPAD
    settings_changed (GST_ELEMENT (self), self->thumbsrcpad);
#endif
}

static void
autofocus_cb (GstOmxCamera *self)
{
    guint32 autofocus_cb_time;

    GstStructure *structure = gst_structure_new ("omx_camera",
            "auto-focus", G_TYPE_BOOLEAN, TRUE, NULL);

    GstMessage *message = gst_message_new_element (GST_OBJECT (self),
            structure);

    gst_element_post_message (GST_ELEMENT (self), message);

    autofocus_cb_time = omap_32k_readraw ();
    GST_CAT_INFO_OBJECT (gstomx_ppm, GST_OBJECT (self), "%d Autofocus locked",
                         autofocus_cb_time);
}

static void
index_settings_changed_cb (GOmxCore *core, gint data1, gint data2)
{
    GstOmxCamera *self = core->object;

    if (data2 == OMX_IndexConfigCommonFocusStatus)
        autofocus_cb (self);
}

static void
setup_ports (GstOmxBaseSrc *base_src)
{
    GstOmxCamera *self = GST_OMX_CAMERA (base_src);
    OMX_PARAM_PORTDEFINITIONTYPE param;

#ifdef USE_GSTOMXCAM_THUMBSRCPAD
    G_OMX_PORT_GET_DEFINITION (self->vid_port, &param);
    g_omx_port_setup (self->vid_port, &param);
#endif

#ifdef USE_GSTOMXCAM_IMGSRCPAD
    G_OMX_PORT_GET_DEFINITION (self->img_port, &param);
    g_omx_port_setup (self->img_port, &param);
#endif

#ifdef USE_GSTOMXCAM_IN_PORT
    G_OMX_PORT_GET_DEFINITION (self->in_port, &param);
    g_omx_port_setup (self->in_port, &param);
#endif
}

static GstStateChangeReturn
change_state (GstElement *element,
              GstStateChange transition)
{
    GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;
    GstOmxCamera *self;
    GOmxCore *core;
    GstOmxBaseSrc *omx_base;

    self = GST_OMX_CAMERA (element);
    omx_base = GST_OMX_BASE_SRC (self);
    core = omx_base->gomx;

    GST_INFO_OBJECT (self, "begin: changing state %s -> %s",
                     gst_element_state_get_name (GST_STATE_TRANSITION_CURRENT (transition)),
                     gst_element_state_get_name (GST_STATE_TRANSITION_NEXT (transition)));

    switch (transition)
    {
        case GST_STATE_CHANGE_NULL_TO_READY:
            g_omx_core_init (core);
            if (core->omx_state != OMX_StateLoaded)
            {
                ret = GST_STATE_CHANGE_FAILURE;
                goto leave;
            }
            gst_omx_camera_set_device (self, self->device);
            gst_omx_camera_setup_stereo (self);
            break;
        case GST_STATE_CHANGE_PLAYING_TO_PAUSED:
            /* We have a fairly high chance that, when used in camerabin2,
               we're stopped while one of our ports is currently blocking
               in request_buffer, waiting for the async queue to be filled.
               Since this is done in create, and thus with the LIVE_LOCK
               taken, we need to break it here before we call the base
               class' change_state, which will acquire the LIVE_LOCK too. */
            g_omx_core_flush_start (core);
            g_omx_core_flush_stop (core);
            break;
        default:
            break;
    }

    ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);

leave:
    GST_LOG_OBJECT (self, "end");

    return ret;
}

static GstClockTime
get_timestamp (GstOmxCamera *self)
{
    GstClock *clock;
    GstClockTime timestamp;

    /* timestamps, LOCK to get clock and base time. */
    GST_OBJECT_LOCK (self);
    if ((clock = GST_ELEMENT_CLOCK (self))) {
      /* we have a clock, get base time and ref clock */
      timestamp = GST_ELEMENT (self)->base_time;
      gst_object_ref (clock);
    } else {
      /* no clock, can't set timestamps */
      timestamp = GST_CLOCK_TIME_NONE;
    }
    GST_OBJECT_UNLOCK (self);

    if (clock) {
      GstClockTime latency = get_latency (self);
      timestamp = gst_clock_get_time (clock) - timestamp;
      gst_object_unref (clock);

      if (GST_CLOCK_TIME_IS_VALID (latency)) {
        if (timestamp > latency)
          timestamp -= latency;
        else
          timestamp = 0;
      }
    }

    return timestamp;
}

#ifdef USE_GSTOMXCAM_IMGSRCPAD
/** This function configure the camera component on capturing/no capturing mode **/
static void
set_capture (GstOmxCamera *self, gboolean capture_mode)
{
    OMX_CONFIG_BOOLEANTYPE param;
    GOmxCore *gomx;
    GstOmxBaseSrc *omx_base = GST_OMX_BASE_SRC (self);

    gomx = (GOmxCore *) omx_base->gomx;

    _G_OMX_INIT_PARAM (&param);

    param.bEnabled = (capture_mode == TRUE) ? OMX_TRUE : OMX_FALSE;

    G_OMX_CORE_SET_CONFIG (gomx, OMX_IndexConfigCapturing, &param);

    GST_DEBUG_OBJECT (self, "Capture = %d", param.bEnabled);
}
#endif


static gboolean
start_ports (GstOmxCamera *self)
{
    GstOmxBaseSrc *omx_base = GST_OMX_BASE_SRC (self);

    if (config[self->mode] & PORT_PREVIEW)
    {
        GST_DEBUG_OBJECT (self, "enable preview port");
        if (!g_omx_port_enable (omx_base->out_port))
            return FALSE;
    }

#ifdef USE_GSTOMXCAM_THUMBSRCPAD
    if (config[self->mode] & PORT_VIDEO)
    {
        GST_DEBUG_OBJECT (self, "enable video port");
        if (!g_omx_port_enable (self->vid_port))
            return FALSE;
    }
#endif

#ifdef USE_GSTOMXCAM_IMGSRCPAD
    if (config[self->mode] & PORT_IMAGE)
    {
        guint32 capture_start_time;

        GST_DEBUG_OBJECT (self, "enable image port");

        /* WORKAROUND: Image capture set only in LOADED state */
        /* set_camera_operating_mode (self); */
        if (!g_omx_port_enable (self->img_port))
            return FALSE;

        GST_DEBUG_OBJECT (self, "image port set_capture set to  %d", TRUE);

        capture_start_time = omap_32k_readraw();
        GST_CAT_INFO_OBJECT (gstomx_ppm, self, "%d Start Image Capture",
                             capture_start_time);

        /* We should logically set the camera to start capturing here,
           but doing so errors out with a "cannot capture yet" error.
           Experimentation shows that this step has to be done after
           the preview port has already been poked for an image. */
        /* set_capture (self, TRUE); */
    }
#endif

    return TRUE;
}


static void
stop_ports (GstOmxCamera *self)
{
    GstOmxBaseSrc *omx_base = GST_OMX_BASE_SRC (self);
    if (config[self->mode] & PORT_PREVIEW)
    {
        GST_DEBUG_OBJECT (self, "disable preview port");
        g_omx_port_disable (omx_base->out_port);
    }

#ifdef USE_GSTOMXCAM_THUMBSRCPAD
    if (config[self->mode] & PORT_VIDEO)
    {
        GST_DEBUG_OBJECT (self, "disable video port");
        g_omx_port_disable (self->vid_port);
    }
#endif

#ifdef USE_GSTOMXCAM_IMGSRCPAD
    if (config[self->mode] & PORT_IMAGE)
    {
        GST_DEBUG_OBJECT (self, "disable image port");
        g_omx_port_disable (self->img_port);
        set_capture (self, FALSE);
    }
#endif
}

#define CALC_RELATIVE(mult, image_size, chunk_size) ((mult * chunk_size) / image_size)


/*
 * GstBaseSrc Methods:
 */
static gboolean
stop (GstBaseSrc *gst_base)
{
    gboolean res;
    GstOmxCamera *self = GST_OMX_CAMERA (gst_base);

    res = GST_BASE_SRC_CLASS (parent_class)->stop (gst_base);
    if (!res)
        return FALSE;

    self->next_mode = self->mode;
    self->mode = -1;
    self->fps_n = 0;
    self->fps_d = 0;
    self->latency = GST_CLOCK_TIME_NONE;
    create_ports (self);

    return TRUE;
}

static GstFlowReturn
create (GstBaseSrc *gst_base,
        guint64 offset,
        guint length,
        GstBuffer **ret_buf)
{
    GstOmxCamera *self = GST_OMX_CAMERA (gst_base);
    GstOmxBaseSrc *omx_base = GST_OMX_BASE_SRC (self);
    GstBuffer *preview_buf = NULL;
    GstBuffer *vid_buf = NULL;
    GstBuffer *img_buf = NULL;
    GstBuffer *thumb_buf = NULL;
    GstFlowReturn ret = GST_FLOW_NOT_NEGOTIATED;
    GstClockTime timestamp;
    GstEvent *vstab_evt = NULL;
    gboolean pending_eos;
    static guint cont;
    gboolean do_delayed_capture = FALSE;

    pending_eos = g_atomic_int_compare_and_exchange (&self->pending_eos, TRUE, FALSE);

    GST_DEBUG_OBJECT (self, "begin, mode=%d, pending_eos=%d", self->mode, pending_eos);

    GST_LOG_OBJECT (self, "state: %d", omx_base->gomx->omx_state);

    if (self->mode != self->next_mode)
    {
        GST_DEBUG_OBJECT (self, "switching from mode %d to mode %d",
            self->mode, self->next_mode);
        if (self->mode != -1) {
            stop_ports (self);
            g_omx_core_stop (omx_base->gomx);
            g_omx_core_unload (omx_base->gomx);
        }

        set_camera_operating_mode (self);
        gst_omx_base_src_setup_ports (omx_base);
        setup_pools (self);
        g_omx_core_prepare (omx_base->gomx);
        self->mode = self->next_mode;
        if (!start_ports (self)) {
            GST_WARNING_OBJECT (self, "Failed to start ports");
            ret = GST_FLOW_ERROR;
            goto fail;
        }
        do_delayed_capture = TRUE;

        /* @todo for now just capture one image... later let the user config
         * this to the number of desired burst mode images
         */
        if (self->mode == MODE_IMAGE || self->mode == MODE_VIDEO_IMAGE)
            self->img_count = 1;
        if (self->mode == MODE_IMAGE_HS)
            self->img_count = self->img_port->num_buffers;
    }

    if (config[self->mode] & PORT_PREVIEW)
    {
        ret = gst_omx_base_src_create_from_port (omx_base,
                omx_base->out_port, &preview_buf);
        if (ret != GST_FLOW_OK)
            goto fail;
        if (self->mode == MODE_VIDEO)
        {
            vid_buf = gst_buffer_ref (preview_buf);
        }
    }

#ifdef USE_GSTOMXCAM_THUMBSRCPAD
    if (config[self->mode] & PORT_VIDEO)
    {
        ret = gst_omx_base_src_create_from_port (omx_base,
                self->vid_port, &thumb_buf);
        if (ret != GST_FLOW_OK)
            goto fail;
    }
#endif

#ifdef USE_GSTOMXCAM_IMGSRCPAD
    if (config[self->mode] & PORT_IMAGE)
    {
        if (do_delayed_capture) {
            GST_DEBUG ("Delayed capture enablement");
            set_capture (self, TRUE);
        }

        ret = gst_omx_base_src_create_from_port (omx_base,
                self->img_port, &img_buf);
        if (ret != GST_FLOW_OK)
            goto fail;

        if (--self->img_count == 0)
        {
            self->next_mode = MODE_PREVIEW;
            GST_DEBUG_OBJECT (self, "image port set_capture set to %d", FALSE);
            set_capture (self, FALSE);
        }
        GST_DEBUG_OBJECT (self, "### img_count = %d ###", self->img_count);
    }
#endif

    timestamp = get_timestamp (self);
    cont ++;
    GST_DEBUG_OBJECT (self, "******** preview buffers cont = %d", cont);
    GST_BUFFER_TIMESTAMP (preview_buf) = timestamp;

    *ret_buf = preview_buf;

    if (vid_buf)
    {
#ifdef USE_GSTOMXCAM_VIDSRCPAD
        GST_DEBUG_OBJECT (self, "pushing vid_buf %"GST_PTR_FORMAT, vid_buf);
        GST_BUFFER_TIMESTAMP (vid_buf) = timestamp;
        if (vstab_evt)
            gst_pad_push_event (self->vidsrcpad, gst_event_ref (vstab_evt));
        gst_buffer_set_caps (vid_buf, GST_PAD_CAPS (gst_base->srcpad));
        gst_pad_push (self->vidsrcpad, vid_buf);
        if (G_UNLIKELY (pending_eos))
            gst_pad_push_event (self->vidsrcpad, gst_event_new_eos ());
#else
        gst_buffer_unref (vid_buf);
#endif
    }

    if (img_buf)
    {
        GST_DEBUG_OBJECT (self, "pushing img_buf %"GST_PTR_FORMAT, img_buf);
        GST_BUFFER_TIMESTAMP (img_buf) = timestamp;
        gst_pad_push (self->imgsrcpad, img_buf);
        if (G_UNLIKELY (pending_eos))
            gst_pad_push_event (self->imgsrcpad, gst_event_new_eos ());
    }

    if (thumb_buf)
    {
        GST_DEBUG_OBJECT (self, "pushing thumb_buf %"GST_PTR_FORMAT, thumb_buf);
        GST_BUFFER_TIMESTAMP (thumb_buf) = timestamp;
        gst_pad_push (self->thumbsrcpad, thumb_buf);
        if (G_UNLIKELY (pending_eos))
            gst_pad_push_event (self->thumbsrcpad, gst_event_new_eos ());
    }

    if (vstab_evt)
    {
        gst_event_unref (vstab_evt);
    }

    if (G_UNLIKELY (pending_eos))
    {
         /* now send eos event, which was previously deferred, to parent
          * class this will trigger basesrc's eos logic.  Unfortunately we
          * can't call parent->send_event() directly from here to pass along
          * the eos, which would be a more obvious approach, because that
          * would deadlock when it tries to acquire live-lock.. but live-
          * lock is already held when calling create().
          */
          return GST_FLOW_UNEXPECTED;
    }

    GST_DEBUG_OBJECT (self, "end, ret=%d", ret);

    return GST_FLOW_OK;

fail:
    if (preview_buf) gst_buffer_unref (preview_buf);
    if (vid_buf)     gst_buffer_unref (vid_buf);
    if (img_buf)     gst_buffer_unref (img_buf);
    if (thumb_buf)   gst_buffer_unref (thumb_buf);

    return ret;
}

static gboolean
send_event (GstElement * element, GstEvent * event)
{
    GstOmxCamera *self = GST_OMX_CAMERA (element);

    GST_DEBUG_OBJECT (self, "received %s event", GST_EVENT_TYPE_NAME (event));

    switch (GST_EVENT_TYPE (event))
    {
        case GST_EVENT_EOS:
            /* note: we don't pass the eos event on to basesrc until
             * we have a chance to handle it ourselves..
             */
            g_atomic_int_set (&self->pending_eos, TRUE);
            gst_event_unref (event);
            return TRUE;
        default:
            return GST_ELEMENT_CLASS (parent_class)->send_event (element, event);
    }
}

static void
finalize (GObject *obj)
{
    GstOmxCamera *self;

    self = GST_OMX_CAMERA (obj);

    if (self->buffer_pool)
        gst_drm_buffer_pool_destroy (self->buffer_pool);
    if (self->omap_dev)
      dce_deinit (self->omap_dev);
    if (self->channels)
        g_list_free_full (self->channels, g_object_unref);

    G_OBJECT_CLASS (parent_class)->finalize (obj);
}

/* Negotiates a particular source pad. Adapted from the basesrc code. */
static gboolean
negotiate_pad (GstOmxCamera * self, GstPad * pad)
{
  GstCaps *thiscaps;
  GstCaps *caps = NULL;
  GstCaps *peercaps = NULL;
  gboolean result = FALSE;

  /* first see what is possible on our source pad */
  thiscaps = gst_pad_get_caps_reffed (pad);
  GST_DEBUG_OBJECT (pad, "caps of src: %" GST_PTR_FORMAT, thiscaps);
  /* nothing or anything is allowed, we're done */
  if (thiscaps == NULL || gst_caps_is_any (thiscaps))
    goto no_nego_needed;

  if (G_UNLIKELY (gst_caps_is_empty (thiscaps)))
    goto no_caps;

  /* get the peer caps */
  peercaps = gst_pad_peer_get_caps_reffed (pad);
  GST_DEBUG_OBJECT (pad, "caps of peer: %" GST_PTR_FORMAT, peercaps);
  if (peercaps) {
    /* get intersection */
    caps =
        gst_caps_intersect_full (peercaps, thiscaps, GST_CAPS_INTERSECT_FIRST);
    GST_DEBUG_OBJECT (pad, "intersect: %" GST_PTR_FORMAT, caps);
    gst_caps_unref (peercaps);
  } else {
    /* no peer, work with our own caps then */
    caps = gst_caps_copy (thiscaps);
  }
  gst_caps_unref (thiscaps);
  if (caps) {
    /* take first (and best, since they are sorted) possibility */
    gst_caps_truncate (caps);

    /* now fixate */
    if (!gst_caps_is_empty (caps)) {
      gst_pad_fixate_caps (pad, caps);
      GST_DEBUG_OBJECT (pad, "fixated to: %" GST_PTR_FORMAT, caps);

      if (gst_caps_is_any (caps)) {
        /* hmm, still anything, so element can do anything and
         * nego is not needed */
        result = TRUE;
      } else if (gst_caps_is_fixed (caps)) {
        /* yay, fixed caps, use those then, it's possible that the subclass does
         * not accept this caps after all and we have to fail. */
        result = gst_pad_set_caps (pad, caps);
      }
    }
    gst_caps_unref (caps);
  } else {
    GST_DEBUG_OBJECT (pad, "no common caps");
  }
  return result;

no_nego_needed:
  {
    GST_DEBUG_OBJECT (pad, "no negotiation needed");
    if (thiscaps)
      gst_caps_unref (thiscaps);
    return TRUE;
  }
no_caps:
  {
    GST_ELEMENT_ERROR (self, STREAM, FORMAT,
        ("No supported formats found"),
        ("This element did not produce valid caps"));
    if (thiscaps)
      gst_caps_unref (thiscaps);
    return TRUE;
  }

}

static gboolean
negotiate (GstBaseSrc * basesrc)
{
    GstOmxCamera *self;
    gboolean ret;

    self = GST_OMX_CAMERA (basesrc);
    GST_DEBUG_OBJECT (self, "called");

    /* We fist perform the normal basesrc negotation, then do a similar
       process for the other pads */
    ret = GST_BASE_SRC_CLASS (parent_class)->negotiate (basesrc);
    if (!ret) {
        GST_DEBUG_OBJECT (self, "Base class failed to negotiate");
        return FALSE;
    }

#ifdef USE_GSTOMXCAM_VIDSRCPAD
    ret = negotiate_pad (self, self->vidsrcpad);
    if (!ret) {
        GST_DEBUG_OBJECT (self, "vidsrcpad failed to negotiate");
        return FALSE;
    }
#endif

#ifdef USE_GSTOMXCAM_IMGSRCPAD
    ret = negotiate_pad (self, self->imgsrcpad);
    if (!ret) {
        GST_DEBUG_OBJECT (self, "imgsrcpad failed to negotiate");
        return FALSE;
    }
#endif

#ifdef USE_GSTOMXCAM_THUMBSRCPAD
    ret = negotiate_pad (self, self->thumbsrcpad);
    if (!ret) {
        GST_DEBUG_OBJECT (self, "thumbsrcpad failed to negotiate");
        return FALSE;
    }
#endif

    return TRUE;
}

/*
 * Initialization:
 */

static void
type_base_init (gpointer g_class)
{
    GstElementClass *element_class;

    element_class = GST_ELEMENT_CLASS (g_class);

    gst_element_class_set_details (element_class, &element_details);

    gst_element_class_add_pad_template (element_class,
        gst_static_pad_template_get (&src_template));

#ifdef USE_GSTOMXCAM_VIDSRCPAD
    gst_element_class_add_pad_template (element_class,
        gst_static_pad_template_get (&vidsrc_template));
#endif

#ifdef USE_GSTOMXCAM_IMGSRCPAD
    gst_element_class_add_pad_template (element_class,
        gst_static_pad_template_get (&imgsrc_template));
#endif

#ifdef USE_GSTOMXCAM_THUMBSRCPAD
    gst_element_class_add_pad_template (element_class,
        gst_static_pad_template_get (&thumbsrc_template));
#endif

#if 0
    gst_element_class_add_pad_template (element_class,
        gst_static_pad_template_get (&sink_template));
#endif
}

static void
type_class_init (gpointer g_class,
                 gpointer class_data)
{
    GObjectClass *gobject_class = G_OBJECT_CLASS (g_class);
    GstElementClass *gst_element_class = GST_ELEMENT_CLASS (g_class);
    GstBaseSrcClass *gst_base_src_class = GST_BASE_SRC_CLASS (g_class);
    GstOmxBaseSrcClass *omx_base_class = GST_OMX_BASE_SRC_CLASS (g_class);

    omx_base_class->out_port_index = OMX_CAMERA_PORT_VIDEO_OUT_PREVIEW;

    /* GstBaseSrc methods: */
    gst_base_src_class->create = GST_DEBUG_FUNCPTR (create);
    gst_base_src_class->stop = GST_DEBUG_FUNCPTR (stop);
    gst_base_src_class->negotiate = GST_DEBUG_FUNCPTR (negotiate);

    /* GstElement methods: */
    gst_element_class->send_event = GST_DEBUG_FUNCPTR (send_event);
    gst_element_class->change_state = change_state;

    /* GObject methods: */
    gobject_class->set_property = GST_DEBUG_FUNCPTR (set_property);
    gobject_class->get_property = GST_DEBUG_FUNCPTR (get_property);

    /* install properties: */
    install_camera_properties (gobject_class);

    gobject_class->finalize = GST_DEBUG_FUNCPTR (finalize);
}


void check_settings (GOmxPort *port, GstPad *pad);


/**
 * overrides the default buffer allocation for thumb_port to allow
 * pad_alloc'ing from the thumbsrcpad
 */
static GstBuffer *
thumb_buffer_alloc (GOmxPort *port, gint len)
{
    GstOmxCamera *self = port->core->object;
    GstBuffer *buf;
    GstFlowReturn ret;

    GST_DEBUG_OBJECT (self, "thumb_buffer_alloc begin");
    check_settings (self->vid_port, self->thumbsrcpad);

    ret = gst_pad_alloc_buffer_and_set_caps (
            self->thumbsrcpad, GST_BUFFER_OFFSET_NONE,
            len, GST_PAD_CAPS (self->thumbsrcpad), &buf);

    if (ret == GST_FLOW_OK) return buf;

    return NULL;
}

static void
do_local_eos (GstOmxCamera * self)
{
    /* Always send the EOS, as the pads are ALWAYS pads, and mode can
       change at runtime, which means we may not EOS in the same mode
       as we started with */
#ifdef USE_GSTOMXCAM_VIDSRCPAD
    gst_pad_push_event (self->vidsrcpad, gst_event_new_eos ());
#endif
#ifdef USE_GSTOMXCAM_IMGSRCPAD
    gst_pad_push_event (self->imgsrcpad, gst_event_new_eos ());
#endif
#ifdef USE_GSTOMXCAM_THUMBSRCPAD
    gst_pad_push_event (self->thumbsrcpad, gst_event_new_eos ());
#endif
}

static gboolean
event_probe (GstPad * pad, GstMiniObject * o, gpointer userdata)
{
    GstOmxCamera * self = GST_OMX_CAMERA (userdata);

    if (GST_IS_EVENT (o)) {
        GstEvent *ev = GST_EVENT (o);
        if (GST_EVENT_TYPE (ev) == GST_EVENT_EOS) {
            do_local_eos (self);
        }
    }
    return TRUE;
}

static void
setup_event_probe (GstOmxCamera * self)
{
    /* This is mostly for the EOS event.
     * When the GstBaseSrc task pauses on EOS, it sends an EOS event
     * to the GstBaseSrc source pad, but does not notify the derived
     * class. This means that the parent bin will be left waiting on
     * any other pads that may be linked, which will hang as soon as
     * one of our own pads is linked. The only way I can see to tell
     * that this happens is to set an event probe on the main pad to
     * detect when an EOS event is sent through it, so we can send a
     * separate EOS event on each of our own pads.
     */
     gst_pad_add_event_probe (GST_BASE_SRC_PAD (self), (GCallback)event_probe, self);
}

static void
type_instance_init (GTypeInstance *instance,
                    gpointer g_class)
{
    GstOmxCamera   *self     = GST_OMX_CAMERA (instance);
    GstOmxBaseSrc  *omx_base = GST_OMX_BASE_SRC (self);
    GstBaseSrc     *basesrc  = GST_BASE_SRC (self);
    GstPadTemplate *pad_template;
    GstColorBalanceChannel *channel;
    static const gchar * const channels[3] = { "SATURATION",
      "CONTRAST", "SHARPNESS"
    };
    gint i;

    GST_DEBUG_OBJECT (omx_base, "begin");

    g_omx_core_init (omx_base->gomx);
    if (omx_base->gomx->omx_state != OMX_StateLoaded)
    {
        GST_ELEMENT_ERROR (self, LIBRARY, INIT,
            ("Failed to initialize OMX"),
            ("Failed to initialize OMX"));
        return;
    }

    self->mode = -1;
    self->next_mode = MODE_PREVIEW;
    self->drop_late_buffers = TRUE;

    omx_base->setup_ports = setup_ports;

    omx_base->gomx->settings_changed_cb = settings_changed_cb;
    omx_base->gomx->index_settings_changed_cb = index_settings_changed_cb;

    omx_base->gomx->use_timestamps = TRUE;

    create_ports (self);

    gst_base_src_set_live (basesrc, TRUE);

    /* setup src pad (already created by basesrc): */

    gst_pad_set_setcaps_function (basesrc->srcpad,
            GST_DEBUG_FUNCPTR (src_setcaps));
    gst_pad_set_fixatecaps_function (basesrc->srcpad,
            GST_DEBUG_FUNCPTR (src_fixatecaps));

    gst_pad_set_query_function (basesrc->srcpad,
            GST_DEBUG_FUNCPTR (src_query));

#ifdef USE_GSTOMXCAM_VIDSRCPAD
    /* create/setup vidsrc pad: */
    pad_template = gst_element_class_get_pad_template (
            GST_ELEMENT_CLASS (g_class), "vidsrc");
    g_return_if_fail (pad_template != NULL);

    GST_DEBUG_OBJECT (basesrc, "creating vidsrc pad");
    self->vidsrcpad = gst_pad_new_from_template (pad_template, "vidsrc");
    gst_element_add_pad (GST_ELEMENT_CAST (self), self->vidsrcpad);
    gst_pad_set_query_function (self->vidsrcpad,
            GST_DEBUG_FUNCPTR (src_query));
#endif

#ifdef USE_GSTOMXCAM_IMGSRCPAD
    /* create/setup imgsrc pad: */
    pad_template = gst_element_class_get_pad_template (
            GST_ELEMENT_CLASS (g_class), "imgsrc");
    g_return_if_fail (pad_template != NULL);

    GST_DEBUG_OBJECT (basesrc, "creating imgsrc pad");
    self->imgsrcpad = gst_pad_new_from_template (pad_template, "imgsrc");
    gst_element_add_pad (GST_ELEMENT_CAST (self), self->imgsrcpad);
    gst_pad_set_setcaps_function (self->imgsrcpad,
            GST_DEBUG_FUNCPTR (imgsrc_setcaps));
    gst_pad_set_fixatecaps_function (self->imgsrcpad,
            GST_DEBUG_FUNCPTR (imgsrc_fixatecaps));
    gst_pad_set_event_function (self->imgsrcpad,
            GST_DEBUG_FUNCPTR (imgsrc_event));
#endif

#ifdef USE_GSTOMXCAM_THUMBSRCPAD
    /* create/setup thumbsrc pad: */
    pad_template = gst_element_class_get_pad_template (
            GST_ELEMENT_CLASS (g_class), "thumbsrc");
    g_return_if_fail (pad_template != NULL);

    GST_DEBUG_OBJECT (basesrc, "creating thumbsrc pad");
    self->thumbsrcpad = gst_pad_new_from_template (pad_template, "thumbsrc");
    gst_element_add_pad (GST_ELEMENT_CAST (self), self->thumbsrcpad);
    gst_pad_set_setcaps_function (self->thumbsrcpad,
            GST_DEBUG_FUNCPTR (thumbsrc_setcaps));
#endif

    setup_event_probe (self);

    /* set colorbalance channels*/

#if 0
    self->contrast = DEFAULT_PROP_CONTRAST;
    self->brightness = DEFAULT_PROP_BRIGHTNESS;
    self->hue = DEFAULT_PROP_HUE;
    self->saturation = DEFAULT_PROP_SATURATION;
#endif

    g_assert (self->channels == NULL);
    for (i = 0; i < G_N_ELEMENTS (channels); i++) {

      channel = g_object_new (GST_TYPE_COLOR_BALANCE_CHANNEL, NULL);
      channel->label = g_strdup (channels[i]);
      channel->min_value = -100;
      channel->max_value = 100;

      self->channels = g_list_append (self->channels, channel);
    }

    channel = g_object_new (GST_TYPE_COLOR_BALANCE_CHANNEL, NULL);
    channel->label = g_strdup ("BRIGHTNESS");
    channel->min_value = 0;
    channel->max_value = 100;
    self->channels = g_list_append (self->channels, channel);

    self->fps_n = 0;
    self->fps_d = 0;
    self->latency = -1;

    self->omap_dev = dce_init ();
    if (self->omap_dev == NULL) {
      GST_ERROR("open failed");
    }
    self->drm_fd = dce_get_fd ();

    GST_DEBUG_OBJECT (omx_base, "end");
}

static void
create_ports (GstOmxCamera *self)
{
    GstOmxBaseSrc *omx_base = GST_OMX_BASE_SRC (self);

    GST_DEBUG_OBJECT (omx_base, "begin");
    self->vid_port = g_omx_core_get_port (omx_base->gomx, "vid",
            OMX_CAMERA_PORT_VIDEO_OUT_VIDEO);
    self->img_port = g_omx_core_get_port (omx_base->gomx, "img",
        OMX_CAMERA_PORT_IMAGE_OUT_IMAGE);
    self->in_port = g_omx_core_get_port (omx_base->gomx, "in",
            OMX_CAMERA_PORT_OTHER_IN);
    self->in_vid_port = g_omx_core_get_port (omx_base->gomx, "in_vid",
            OMX_CAMERA_PORT_VIDEO_IN_VIDEO);
    self->msr_port = g_omx_core_get_port (omx_base->gomx, "msr",
            OMX_CAMERA_PORT_VIDEO_OUT_MEASUREMENT);

    self->vid_port->buffer_alloc = thumb_buffer_alloc;

    omx_base->out_port->drop_late_buffers = self->drop_late_buffers;

    g_object_set (self, "allocate-buffers", FALSE, NULL);
#if 0
    self->in_port = g_omx_core_get_port (omx_base->gomx, "in"
            OMX_CAMERA_PORT_VIDEO_IN_VIDEO);
#endif



#if 0
    /* disable all ports to begin with: */
    g_omx_port_disable (self->in_port);
#endif
    GST_DEBUG_OBJECT (omx_base, "Disabling ports");
    g_omx_port_disable (omx_base->out_port);
    g_omx_port_disable (self->vid_port);
    g_omx_port_disable (self->img_port);
    g_omx_port_disable (self->in_port);
    g_omx_port_disable (self->in_vid_port);
    g_omx_port_disable (self->msr_port);

    GST_DEBUG_OBJECT (omx_base, "end");
}
