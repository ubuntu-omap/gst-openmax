/* GStreamer
 *
 * Copyright (C) 2009 Texas Instruments, Inc - http://www.ti.com/
 * Copyright (C) 2011 Collabora Ltda
 *  @author: Luciana Fujii Pontello <luciana.fujii@collabora.co.uk>
 *
 * Description: OMX Camera element
 *  Created on: Mar 22, 2011
 *      Author: Joaquin Castellanos <jcastellanos@ti.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "gstomx_camera_parameters.h"
#include "gstomx.h"
#include "timer-32k.h"

#ifdef USE_OMXTICORE
#  include <OMX_TI_IVCommon.h>
#  include <OMX_TI_Index.h>
#endif

#define GST_USE_UNSTABLE_API TRUE

#include <stdint.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <OMX_CoreExt.h>
#include <OMX_IndexExt.h>
#ifdef USE_OMXTICORE
#include "OMX_IVCommon.h"
#else
#include <omx/OMX_IVCommon.h>
#endif
#include <gst/interfaces/photography.h>

/*
 * Enums:
 */

GType
gst_omx_camera_mode_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static GEnumValue vals[] =
        {
            {MODE_PREVIEW,        "Preview",                    "preview"},
            {MODE_VIDEO,          "Video Capture",              "video"},
            {MODE_VIDEO_IMAGE,    "Video+Image Capture",        "video-image"},
            {MODE_IMAGE,          "Image Capture",              "image"},
            {MODE_IMAGE_HS,       "Image Capture High Speed",   "image-hs"},
            {0, NULL, NULL},
        };

        type = g_enum_register_static ("GstOmxCameraMode", vals);
    }

    return type;
}

/*
 * OMX Structure wrappers
 */
GType
gst_omx_camera_shutter_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static GEnumValue vals[] =
        {
            {SHUTTER_OFF,         "Off",                        "off"},
            {SHUTTER_HALF_PRESS,  "Half Press",                 "half-press"},
            {SHUTTER_FULL_PRESS,  "Full Press",                 "full-press"},
            {0, NULL, NULL},
        };

        type = g_enum_register_static ("GstOmxCameraShutter", vals);
    }

    return type;
}

GType
gst_omx_camera_focus_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static GEnumValue vals[] =
        {
            {OMX_IMAGE_FocusControlOff,      "off",              "off"},
            {OMX_IMAGE_FocusControlOn,       "on",               "on"},
            {OMX_IMAGE_FocusControlAuto,     "auto",             "auto"},
            {OMX_IMAGE_FocusControlAutoLock, "autolock",         "autolock"},
#ifdef USE_OMXTICORE
            {OMX_IMAGE_FocusControlAutoMacro,         "AutoMacro",      "automacro"},
            {OMX_IMAGE_FocusControlAutoInfinity,      "AutoInfinity",   "autoinfinity"},
            {OMX_IMAGE_FocusControlHyperfocal,        "Hyperfocal",     "hyperfocal"},
            {OMX_IMAGE_FocusControlPortrait,          "Portrait",       "portrait"},
            {OMX_IMAGE_FocusControlExtended,          "Extended",       "extended"},
            {OMX_IMAGE_FocusControlContinousNormal,   "Cont-Normal",    "cont-normal"},
            {OMX_IMAGE_FocusControlContinousExtended, "Cont-Extended",  "cont-extended"},
#endif
            {0, NULL, NULL},
        };

        type = g_enum_register_static ("GstOmxCameraFocus", vals);
    }

    return type;
}

GType
gst_omx_camera_awb_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static const GEnumValue vals[] =
        {
            {OMX_WhiteBalControlOff,           "Balance off",    "off"},
            {OMX_WhiteBalControlAuto,          "Auto balance",   "auto"},
            {OMX_WhiteBalControlSunLight,      "Sun light",      "sunlight"},
            {OMX_WhiteBalControlCloudy,        "Cloudy",         "cloudy"},
            {OMX_WhiteBalControlShade,         "Shade",          "shade"},
            {OMX_WhiteBalControlTungsten,      "Tungsten",       "tungsten"},
            {OMX_WhiteBalControlFluorescent,   "Fluorescent",    "fluorescent"},
            {OMX_WhiteBalControlIncandescent,  "Incandescent",   "incandescent"},
            {OMX_WhiteBalControlFlash,         "Flash",          "flash" },
            {OMX_WhiteBalControlHorizon,       "Horizon",        "horizon" },
            {0, NULL, NULL },
        };

        type = g_enum_register_static ("GstOmxCameraWhiteBalance",vals);
    }

    return type;
}

GType
gst_omx_camera_exposure_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static const GEnumValue vals[] =
        {
            {OMX_ExposureControlOff,             "Exposure control off",     "off"},
            {OMX_ExposureControlAuto,            "Auto exposure",            "auto"},
            {OMX_ExposureControlNight,           "Night exposure",           "night"},
            {OMX_ExposureControlBackLight,       "Backlight exposure",       "backlight"},
            {OMX_ExposureControlSpotLight,       "SportLight exposure",      "sportlight"},
            {OMX_ExposureControlSports,          "Sports exposure",          "sports"},
            {OMX_ExposureControlSnow,            "Snow exposure",            "snow"},
            {OMX_ExposureControlBeach,           "Beach exposure",           "beach"},
            {OMX_ExposureControlLargeAperture,   "Large aperture exposure",  "large-aperture"},
            {OMX_ExposureControlSmallApperture,  "Small aperture exposure",  "small-aperture"},
            {0, NULL, NULL },
        };

        type = g_enum_register_static ("GstOmxCameraExposureControl", vals);
    }

    return type;
}

GType
gst_omx_camera_mirror_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static GEnumValue vals[] =
        {
            {OMX_MirrorNone,        "Off",              "off"},
            {OMX_MirrorVertical,    "Vertical",         "vertical"},
            {OMX_MirrorHorizontal,  "Horizontal",       "horizontal"},
            {OMX_MirrorBoth,        "Both",             "both"},
            {0, NULL, NULL},
        };

        type = g_enum_register_static ("GstOmxCameraMirror", vals);
    }

    return type;
}


#ifdef USE_OMXTICORE

GType
gst_omx_camera_flicker_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static const GEnumValue vals[] =
        {
            {OMX_FlickerCancelOff,  "Flicker control off",       "off"},
            {OMX_FlickerCancelAuto, "Auto flicker control",      "auto"},
            {OMX_FlickerCancel50,   "Flicker control for 50Hz",  "flick-50hz"},
            {OMX_FlickerCancel60,   "Flicker control for 60Hz",  "flick-60hz"},
            {0, NULL, NULL },
        };

        type = g_enum_register_static ("GstOmxCameraFlickerCancel", vals);
    }

    return type;
}

GType
gst_omx_camera_scene_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static const GEnumValue vals[] =
        {
            {OMX_Manual,         "Manual settings",         "manual"},
            {OMX_Closeup,        "Closeup settings",        "closeup"},
            {OMX_Portrait,       "Portrait settings",       "portrait"},
            {OMX_Landscape,      "Landscape settings",      "landscape"},
            {OMX_Underwater,     "Underwater settings",     "underwater"},
            {OMX_Sport,          "Sport settings",          "sport"},
            {OMX_SnowBeach,      "SnowBeach settings",      "snowbeach"},
            {OMX_Mood,           "Mood settings",           "mood"},
#if 0       /* The following options are not yet enabled at OMX level */
            {OMX_NightPortrait,  "NightPortrait settings",  "night-portrait"},
            {OMX_NightIndoor,    "NightIndoor settings",    "night-indoor"},
            {OMX_Fireworks,      "Fireworks settings",      "fireworks"},
            /* for still image: */
            {OMX_Document,       "Document settings",       "document"},
            {OMX_Barcode,        "Barcode settings",        "barcode"},
            /* for video: */
            {OMX_SuperNight,     "SuperNight settings",     "supernight"},
            {OMX_Cine,           "Cine settings",           "cine"},
            {OMX_OldFilm,        "OldFilm settings",        "oldfilm"},
#endif
            {0, NULL, NULL},
        };

        type = g_enum_register_static ("GstOmxCameraScene", vals);
    }

    return type;
}

GType
gst_omx_camera_vnf_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static GEnumValue vals[] =
        {
            {OMX_VideoNoiseFilterModeOff,   "off",              "off"},
            {OMX_VideoNoiseFilterModeOn,    "on",               "on"},
            {OMX_VideoNoiseFilterModeAuto,  "auto",             "auto"},
            {0, NULL, NULL},
        };

        type = g_enum_register_static ("GstOmxCameraVnf", vals);
    }

    return type;
}

GType
gst_omx_camera_yuv_range_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static GEnumValue vals[] =
        {
            {OMX_ITURBT601,       "OMX_ITURBT601",              "OMX_ITURBT601"},
            {OMX_Full8Bit,        "OMX_Full8Bit",               "OMX_Full8Bit"},
            {0, NULL, NULL},
        };

        type = g_enum_register_static ("GstOmxCameraYuvRange", vals);
    }

    return type;
}

GType
gst_omx_camera_device_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static GEnumValue vals[] =
        {
            {OMX_PrimarySensor,     "Primary",          "primary"},
            {OMX_SecondarySensor,   "Secondary",        "secondary"},
            {OMX_TI_StereoSensor,   "Stereo",           "stereo"},
            {0, NULL, NULL},
        };

        type = g_enum_register_static ("GstOmxCameraDevice", vals);
    }

    return type;
}

GType
gst_omx_camera_nsf_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static const GEnumValue vals[] =
        {
            {OMX_ISONoiseFilterModeOff,     "nsf control off",    "off"},
            {OMX_ISONoiseFilterModeOn,      "nsf control on",     "on"},
            {OMX_ISONoiseFilterModeAuto,    "nsf control auto",   "auto"},
            {0, NULL, NULL },
        };

        type = g_enum_register_static ("GstOmxCameraISONoiseFilter", vals);
    }

    return type;
}

GType
gst_omx_camera_focusspot_weight_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static const GEnumValue vals[] =
        {
            {OMX_FocusSpotDefault,        "Common focus region",  "default"},
            {OMX_FocusSpotSinglecenter,   "Single center",        "center"},
            {OMX_FocusSpotMultiNormal,    "Multi normal",         "multinormal"},
            {OMX_FocusSpotMultiAverage,   "Multi average",        "multiaverage"},
            {OMX_FocusSpotMultiCenter,    "Multi center",         "multicenter"},
            {0, NULL, NULL },
        };

        type = g_enum_register_static ("GstOmxCameraFocusSpotWeight", vals);
    }

    return type;
}

GType
gst_omx_camera_bce_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static const GEnumValue vals[] =
        {
            {OMX_TI_BceModeOff,     "bce control off",    "off"},
            {OMX_TI_BceModeOn,      "bce control on",     "on"},
            {OMX_TI_BceModeAuto,    "bce control auto",   "auto"},
            {0, NULL, NULL },
        };

        type = g_enum_register_static ("GstOmxCameraBrightnessContrastEnhance", vals);
    }

    return type;
}

#endif


/*
 *  Methods:
 */

#define SET_PROP(fname,port,type,omxtype,idx,Api,set)        \
void                                                         \
gst_omx_camera_set_##fname (GstOmxCamera *self, type value)  \
{                                                            \
    omxtype var;                                             \
    OMX_ERRORTYPE err;                                       \
    GOmxCore *core = (GST_OMX_BASE_SRC (self))->gomx;        \
    OMX_HANDLETYPE handle = g_omx_core_get_handle (core);    \
                                                             \
    GST_DEBUG_OBJECT (self, "[port %u] Setting " #fname      \
        " to %d", port + 0, value);                          \
                                                             \
    _G_OMX_INIT_PARAM (&var);                                \
    port;                                                    \
    err = OMX_Get##Api (handle, idx, &var);                  \
    if (err != OMX_ErrorNone) {                              \
        GST_WARNING_OBJECT (self,                            \
            "Error getting " #idx ": %x (%s)",               \
            err, g_omx_error_to_str (err));                  \
    } else {                                                 \
        set;                                                 \
        GST_DEBUG_OBJECT (self, #fname ": param=%d", value); \
        err = OMX_Set##Api (handle, idx, &var);              \
        if (err != OMX_ErrorNone) {                          \
            GST_WARNING_OBJECT (self,                        \
                "Error setting " #idx ": %x (%s)",           \
                err, g_omx_error_to_str (err));              \
        }                                                    \
    }                                                        \
}

#define GET_PROP(fname,port,type,omxtype,idx,Api,get)        \
void                                                         \
gst_omx_camera_get_##fname (GstOmxCamera *self, type *value) \
{                                                            \
    omxtype var;                                             \
    OMX_ERRORTYPE err;                                       \
    GOmxCore *core = (GST_OMX_BASE_SRC (self))->gomx;        \
    OMX_HANDLETYPE handle = g_omx_core_get_handle (core);    \
                                                             \
    GST_DEBUG_OBJECT (self, "[port %u] Getting " #fname,     \
        port + 0);                                           \
                                                             \
    _G_OMX_INIT_PARAM (&var);                                \
    port;                                                    \
    err = OMX_Get##Api (handle, idx, &var);                  \
    if (err != OMX_ErrorNone) {                              \
        GST_WARNING_OBJECT (self,                            \
            "Error getting " #idx ": %x (%s)",               \
            err, g_omx_error_to_str (err));                  \
    } else {                                                 \
      get;                                                   \
      GST_DEBUG_OBJECT (self, #fname "=%d", *value);         \
    }                                                        \
}

/* Keep the same behavior as to what port(s), if any, is/are used.
 * Some of the routines did not set a port, using a 0 port index
 * as set by the memset done by _G_OMX_INIT_PARAM. This is likely
 * wrong (neither the video nor image ports have a 0 index, and
 * OMX_ALL is 0xffffffff), but we still do the same here, as it
 * will be trivial to replace NOPORT with whatever ends up being
 * the correct port once we know. */
#define IMGPORT var.nPortIndex = self->img_port->port_index
#define OUTPORT var.nPortIndex = (GST_OMX_BASE_SRC (self))->out_port->port_index
#define NOPORT
#define ALL_PORTS var.nPortIndex = OMX_ALL

#define DEFAULT_SET(field) var.field = value
#define DEFAULT_GET(field) *value = var.field

#define SET_CONFIG(fname,port,type,omxtype,idx,set) SET_PROP(fname,port,type,omxtype,idx,Config,set)
#define GET_CONFIG(fname,port,type,omxtype,idx,get) GET_PROP(fname,port,type,omxtype,idx,Config,get)
#define DEF_CONFIG_GETSET(fname,port,type,omxtype,idx,get,set) \
  GET_CONFIG(fname,port,type,omxtype,idx,get) \
  SET_CONFIG(fname,port,type,omxtype,idx,set)
#define DEF_CONFIG(fname,port,type,omxtype,idx,field) \
  DEF_CONFIG_GETSET(fname,port,type,omxtype,idx,DEFAULT_GET(field),DEFAULT_SET(field))

#define SET_PARAM(fname,port,type,omxtype,idx,set) SET_PROP(fname,port,type,omxtype,idx,Parameter,set)
#define GET_PARAM(fname,port,type,omxtype,idx,get) GET_PROP(fname,port,type,omxtype,idx,Parameter,get)
#define DEF_PARAM_GETSET(fname,port,type,omxtype,idx,get,set) \
  GET_PARAM(fname,port,type,omxtype,idx,get) \
  SET_PARAM(fname,port,type,omxtype,idx,set)
#define DEF_PARAM(fname,port,type,omxtype,idx,field) \
  DEF_PARAM_GETSET(fname,port,type,omxtype,idx,DEFAULT_GET(field),DEFAULT_SET(field))

DEF_CONFIG(brightness,NOPORT,gint,OMX_CONFIG_BRIGHTNESSTYPE,OMX_IndexConfigCommonBrightness,nBrightness)
DEF_CONFIG(saturation,NOPORT,gint,OMX_CONFIG_SATURATIONTYPE,OMX_IndexConfigCommonSaturation,nSaturation)
DEF_CONFIG(contrast,NOPORT,gint,OMX_CONFIG_CONTRASTTYPE,OMX_IndexConfigCommonContrast,nContrast)
DEF_CONFIG(device,OUTPORT,gint,OMX_CONFIG_SENSORSELECTTYPE,OMX_TI_IndexConfigSensorSelect,eSensor)
DEF_CONFIG(white_balance_mode,OUTPORT,OMX_WHITEBALCONTROLTYPE,OMX_CONFIG_WHITEBALCONTROLTYPE,
    OMX_IndexConfigCommonWhiteBalance,eWhiteBalControl)
DEF_CONFIG(rotation,IMGPORT,guint,OMX_CONFIG_ROTATIONTYPE,OMX_IndexConfigCommonRotate,nRotation)
DEF_CONFIG(mirror,IMGPORT,OMX_MIRRORTYPE,OMX_CONFIG_MIRRORTYPE,OMX_IndexConfigCommonMirror,eMirror)
DEF_PARAM(qfactor,IMGPORT,guint,OMX_IMAGE_PARAM_QFACTORTYPE,OMX_IndexParamQFactor,nQFactor)
DEF_CONFIG_GETSET(zoom,NOPORT,guint,OMX_CONFIG_SCALEFACTORTYPE,OMX_IndexConfigCommonDigitalZoom,
    do { *value = var.xWidth * 100 / CAM_ZOOM_IN_STEP; } while(0),
    do {OMX_U32 zf = (CAM_ZOOM_IN_STEP * value) / 100; var.xWidth = zf; var.xHeight = zf; } while(0))
DEF_CONFIG_GETSET(manual_focus,OUTPORT,guint,OMX_IMAGE_CONFIG_FOCUSCONTROLTYPE,OMX_IndexConfigFocusControl,
    DEFAULT_GET(nFocusSteps),
    do { var.eFocusControl = OMX_IMAGE_FocusControlOn; var.nFocusSteps = value; } while(0))
#ifdef USE_OMXTICORE
DEF_PARAM(thumbnail_width,IMGPORT,gint,OMX_PARAM_THUMBNAILTYPE,OMX_IndexParamThumbnail,nWidth)
DEF_PARAM(thumbnail_height,IMGPORT,gint,OMX_PARAM_THUMBNAILTYPE,OMX_IndexParamThumbnail,nHeight)
DEF_CONFIG(flicker_mode,NOPORT,OMX_COMMONFLICKERCANCELTYPE,OMX_CONFIG_FLICKERCANCELTYPE,
    OMX_IndexConfigFlickerCancel,eFlickerCancel)
DEF_CONFIG(scene_mode,NOPORT,OMX_SCENEMODETYPE,OMX_CONFIG_SCENEMODETYPE,OMX_TI_IndexConfigSceneMode,eSceneMode)
DEF_PARAM(vnf,OUTPORT,OMX_VIDEONOISEFILTERMODETYPE,OMX_PARAM_VIDEONOISEFILTERTYPE,
    OMX_IndexParamVideoNoiseFilter,eMode)
DEF_PARAM(yuv_range,OUTPORT,OMX_VIDEOYUVRANGETYPE,OMX_PARAM_VIDEOYUVRANGETYPE,
    OMX_IndexParamVideoCaptureYUVRange,eYUVRange)
DEF_PARAM(nsf,NOPORT,OMX_VIDEONOISEFILTERMODETYPE,OMX_PARAM_ISONOISEFILTERTYPE,OMX_IndexParamHighISONoiseFiler,eMode)
DEF_CONFIG(mtis,NOPORT,gboolean,OMX_CONFIG_BOOLEANTYPE,OMX_IndexConfigMotionTriggeredImageStabilisation,bEnabled)
DEF_PARAM(sensor_overclock,NOPORT,gboolean,OMX_CONFIG_BOOLEANTYPE,OMX_TI_IndexParamSensorOverClockMode,bEnabled)
DEF_CONFIG(white_balance_color_temperature,NOPORT,guint,OMX_TI_CONFIG_WHITEBALANCECOLORTEMPTYPE,
    OMX_TI_IndexConfigWhiteBalanceManualColorTemp,nColorTemperature)
DEF_CONFIG(focus_spot_weighting,NOPORT,OMX_TI_CONFIG_FOCUSSPOTMODETYPE,
    OMX_TI_CONFIG_FOCUSSPOTWEIGHTINGTYPE,OMX_TI_IndexConfigFocusSpotWeighting,eMode)
DEF_CONFIG(gbce,NOPORT,OMX_TI_BRIGHTNESSCONTRASTCRTLTYPE,OMX_TI_CONFIG_LOCAL_AND_GLOBAL_BRIGHTNESSCONTRASTTYPE,
    OMX_TI_IndexConfigGlobalBrightnessContrastEnhance,eControl)
DEF_CONFIG(glbce,NOPORT,OMX_TI_BRIGHTNESSCONTRASTCRTLTYPE,OMX_TI_CONFIG_LOCAL_AND_GLOBAL_BRIGHTNESSCONTRASTTYPE,
    OMX_TI_IndexConfigLocalBrightnessContrastEnhance,eControl)
DEF_CONFIG(cac,NOPORT,gboolean,OMX_CONFIG_BOOLEANTYPE,OMX_IndexConfigChromaticAberrationCorrection,bEnabled)
DEF_PARAM(ldc,NOPORT,gboolean,OMX_CONFIG_BOOLEANTYPE,OMX_IndexParamLensDistortionCorrection,bEnabled)
DEF_CONFIG_GETSET(sharpness,OUTPORT,gint,OMX_IMAGE_CONFIG_PROCESSINGLEVELTYPE,
    OMX_IndexConfigSharpeningLevel,DEFAULT_GET(nLevel),
    do {var.nLevel = value; if (var.nLevel == 0) var.bAuto = OMX_TRUE; else var.bAuto = OMX_FALSE; } while(0))
#endif

GstPhotoCaps
gst_omx_camera_photography_get_capabilities (GstPhotography *photo)
{
  return GST_PHOTOGRAPHY_CAPS_EV_COMP |
         GST_PHOTOGRAPHY_CAPS_ISO_SPEED |
         GST_PHOTOGRAPHY_CAPS_WB_MODE |
         GST_PHOTOGRAPHY_CAPS_SCENE |
         GST_PHOTOGRAPHY_CAPS_ZOOM;
}

gboolean
gst_omx_camera_photography_get_ev_compensation (GstPhotography *photo,
        gfloat *evcomp)
{
    OMX_CONFIG_EXPOSUREVALUETYPE config;
    GOmxCore *gomx;
    OMX_ERRORTYPE error_val = OMX_ErrorNone;
    GstOmxCamera *self = GST_OMX_CAMERA (photo);
    GstOmxBaseSrc *omx_base = GST_OMX_BASE_SRC (self);

    gomx = (GOmxCore *) omx_base->gomx;
    _G_OMX_INIT_PARAM (&config);
    error_val = OMX_GetConfig (gomx->omx_handle,
            OMX_IndexConfigCommonExposureValue, &config);
    g_assert (error_val == OMX_ErrorNone);
    GST_DEBUG_OBJECT (self, "xEVCompensation: EVCompensation=%d",
            config.xEVCompensation);

    return TRUE;
}

gboolean
gst_omx_camera_photography_get_iso_speed (GstPhotography *photo, guint *iso_speed)
{
    OMX_CONFIG_EXPOSUREVALUETYPE config;
    GOmxCore *gomx;
    GstOmxBaseSrc *omx_base = GST_OMX_BASE_SRC (photo);
    OMX_ERRORTYPE error_val = OMX_ErrorNone;
    GstOmxCamera *self = GST_OMX_CAMERA (photo);

    gomx = (GOmxCore *) omx_base->gomx;

    _G_OMX_INIT_PARAM (&config);
    error_val = OMX_GetConfig (gomx->omx_handle,
            OMX_IndexConfigCommonExposureValue, &config);
    g_assert (error_val == OMX_ErrorNone);
    GST_DEBUG_OBJECT (self, "ISO Speed: param=%d", config.nSensitivity);
    *iso_speed = config.nSensitivity;

    return TRUE;
}

gboolean
gst_omx_camera_photography_get_white_balance_mode (GstPhotography *photo,
        GstWhiteBalanceMode *wb_mode)
{
    OMX_WHITEBALCONTROLTYPE omx_wb;
    gint convert_wb;

    gst_omx_camera_get_white_balance_mode (GST_OMX_CAMERA (photo), &omx_wb);
    convert_wb = omx_wb - 1;
    if (convert_wb < 0 || convert_wb > 6)
        return FALSE;

    *wb_mode = convert_wb;
    return TRUE;
}

gboolean
gst_omx_camera_photography_get_scene_mode (GstPhotography *photo,
        GstSceneMode *scene_mode)
{
    OMX_SCENEMODETYPE scene_omx_camera;

    gst_omx_camera_get_scene_mode (GST_OMX_CAMERA (photo), &scene_omx_camera);
    if (scene_omx_camera <= 3)
        *scene_mode = scene_omx_camera;
    else if (scene_omx_camera == 5)
        *scene_mode = GST_PHOTOGRAPHY_SCENE_MODE_SPORT;
    else
        /* scene does not exist in photography */
        return FALSE;

    return TRUE;
}

gboolean
gst_omx_camera_photography_get_zoom (GstPhotography *photo, gfloat *zoom)
{
    guint zoom_int_value;

    gst_omx_camera_get_zoom (GST_OMX_CAMERA (photo), &zoom_int_value);
    *zoom = zoom_int_value /700.0 * 9.0;
    return TRUE;
}

static gboolean
flicker_from_gst_photography (GstFlickerReductionMode flicker, OMX_COMMONFLICKERCANCELTYPE *res)
{
  switch (flicker) {
    case GST_PHOTOGRAPHY_FLICKER_REDUCTION_AUTO:
      *res = OMX_FlickerCancelAuto;
      return TRUE;
    case GST_PHOTOGRAPHY_FLICKER_REDUCTION_OFF:
      *res = OMX_FlickerCancelOff;
      return TRUE;
    case GST_PHOTOGRAPHY_FLICKER_REDUCTION_50HZ:
      *res = OMX_FlickerCancel50;
      return TRUE;
    case GST_PHOTOGRAPHY_FLICKER_REDUCTION_60HZ:
      *res = OMX_FlickerCancel60;
      return TRUE;
    default:
      g_warning ("Unsupported flicker enum in gst->OMX conversion");
      return FALSE;
  }
}

static gboolean
flicker_to_gst_photography (OMX_COMMONFLICKERCANCELTYPE flicker, GstFlickerReductionMode *res)
{
  switch (flicker) {
    case OMX_FlickerCancelOff:
      *res = GST_PHOTOGRAPHY_FLICKER_REDUCTION_OFF;
       return TRUE;
    case OMX_FlickerCancelAuto:
      *res = GST_PHOTOGRAPHY_FLICKER_REDUCTION_AUTO;
       return TRUE;
    case OMX_FlickerCancel100:
      GST_WARNING ("100 Hz flicker cancel unsupported by GstPhotography, using 50");
    case OMX_FlickerCancel50:
      *res = GST_PHOTOGRAPHY_FLICKER_REDUCTION_50HZ;
       return TRUE;
    case OMX_FlickerCancel120:
      GST_WARNING ("120 Hz flicker cancel unsupported by GstPhotography, using 60");
    case OMX_FlickerCancel60:
      *res = GST_PHOTOGRAPHY_FLICKER_REDUCTION_60HZ;
       return TRUE;
    default:
      g_warning ("Unsupported flicker enum in OMX->gst conversion");
      return FALSE;
  }
}

gboolean
gst_omx_camera_photography_get_flicker_mode (GstPhotography *photo,
    GstFlickerReductionMode * flicker_mode)
{
  OMX_COMMONFLICKERCANCELTYPE flicker;
  gst_omx_camera_get_flicker_mode (GST_OMX_CAMERA (photo), &flicker);
  return flicker_to_gst_photography (flicker, flicker_mode);
}

gboolean
gst_omx_camera_photography_set_ev_compensation (GstPhotography *photo,
        gfloat evcomp)
{
    OMX_CONFIG_EXPOSUREVALUETYPE config;
    GOmxCore *gomx;
    OMX_ERRORTYPE error_val = OMX_ErrorNone;
    GstOmxCamera *self = GST_OMX_CAMERA (photo);
    GstOmxBaseSrc *omx_base = GST_OMX_BASE_SRC (self);

    gomx = (GOmxCore *) omx_base->gomx;
    _G_OMX_INIT_PARAM (&config);
    error_val = OMX_GetConfig (gomx->omx_handle,
            OMX_IndexConfigCommonExposureValue, &config);
    g_assert (error_val == OMX_ErrorNone);
    /* Converting into Q16 ( X << 16  = X*65536 ) */
    config.xEVCompensation = (OMX_S32) (evcomp * 65536);
    GST_DEBUG_OBJECT (self, "xEVCompensation: value=%f EVCompensation=%d",
            evcomp, config.xEVCompensation);

    G_OMX_CORE_SET_CONFIG (gomx, OMX_IndexConfigCommonExposureValue, &config);

    return TRUE;
}

gboolean
gst_omx_camera_photography_set_iso_speed (GstPhotography *photo, guint iso_speed)
{
    OMX_CONFIG_EXPOSUREVALUETYPE config;
    GOmxCore *gomx;
    OMX_ERRORTYPE error_val = OMX_ErrorNone;
    GstOmxCamera *self = GST_OMX_CAMERA (photo);
    GstOmxBaseSrc *omx_base = GST_OMX_BASE_SRC (self);

    gomx = (GOmxCore *) omx_base->gomx;
    _G_OMX_INIT_PARAM (&config);
    error_val = OMX_GetConfig (gomx->omx_handle,
            OMX_IndexConfigCommonExposureValue, &config);
    g_assert (error_val == OMX_ErrorNone);
    if (iso_speed > 1600)
      return FALSE;
    config.bAutoSensitivity = (iso_speed < 100) ? OMX_TRUE : OMX_FALSE;
    if (config.bAutoSensitivity == OMX_FALSE)
    {
        config.nSensitivity = iso_speed;
    }
    GST_DEBUG_OBJECT (self, "ISO Speed: Auto=%d Sensitivity=%d",
            config.bAutoSensitivity, config.nSensitivity);

    G_OMX_CORE_SET_CONFIG (gomx, OMX_IndexConfigCommonExposureValue, &config);

    return TRUE;
}

gboolean
gst_omx_camera_photography_set_white_balance_mode (GstPhotography *photo,
        GstWhiteBalanceMode wb_mode)
{
    OMX_WHITEBALCONTROLTYPE wb_omx_camera;

    wb_omx_camera = wb_mode + 1;
    gst_omx_camera_set_white_balance_mode (GST_OMX_CAMERA (photo), wb_omx_camera);
    return TRUE;
}

gboolean
gst_omx_camera_photography_set_scene_mode (GstPhotography *photo,
        GstSceneMode scene_mode)
{
    OMX_SCENEMODETYPE scene_omx_camera;

    if (scene_mode <= 3)
        scene_omx_camera = scene_mode;
    else if (scene_mode == GST_PHOTOGRAPHY_SCENE_MODE_SPORT)
        scene_omx_camera = 5;
    else
        /* scene does not exist in omx_camera */
        return FALSE;

    gst_omx_camera_set_scene_mode (GST_OMX_CAMERA (photo), scene_omx_camera);
    return TRUE;
}

gboolean
gst_omx_camera_photography_set_zoom (GstPhotography *photo, gfloat zoom)
{
    guint zoom_int_value;

    zoom_int_value = abs (zoom * 900.0 / 7.0);
    gst_omx_camera_set_zoom (GST_OMX_CAMERA (photo), zoom_int_value);
    return TRUE;
}

gboolean
gst_omx_camera_photography_set_flicker_mode (GstPhotography *photo,
    GstFlickerReductionMode flicker_mode)
{
  OMX_COMMONFLICKERCANCELTYPE flicker;
  if (!flicker_from_gst_photography (flicker_mode, &flicker))
    return FALSE;
  gst_omx_camera_set_flicker_mode (GST_OMX_CAMERA (photo), flicker);
  return TRUE;
}

static void
gst_omx_camera_set_autofocus (GstPhotography *photo,
    OMX_IMAGE_FOCUSCONTROLTYPE focus)
{
    OMX_IMAGE_CONFIG_FOCUSCONTROLTYPE config;
    OMX_CONFIG_CALLBACKREQUESTTYPE focusreq_cb;
    OMX_CONFIG_BOOLEANTYPE b;
    GOmxCore *gomx;
    OMX_ERRORTYPE error_val = OMX_ErrorNone;
    GstOmxCamera *self = GST_OMX_CAMERA (photo);
    GstOmxBaseSrc *omx_base = GST_OMX_BASE_SRC (self);

    gomx = (GOmxCore *) omx_base->gomx;
    _G_OMX_INIT_PARAM (&config);
    _G_OMX_INIT_PARAM (&focusreq_cb);
    _G_OMX_INIT_PARAM (&b);
    error_val = OMX_GetConfig(gomx->omx_handle,
            OMX_IndexConfigFocusControl, &config);
    g_assert (error_val == OMX_ErrorNone);
    config.nPortIndex = omx_base->out_port->port_index;
    config.eFocusControl = focus;
    GST_DEBUG_OBJECT (self, "AF: param=%d port=%d", config.eFocusControl,
            config.nPortIndex);

    G_OMX_CORE_SET_CONFIG (gomx, OMX_IndexConfigFocusControl, &config);

    b.bEnabled = (config.eFocusControl == OMX_IMAGE_FocusControlAuto) ? OMX_TRUE : OMX_FALSE;
    G_OMX_CORE_SET_CONFIG (gomx, OMX_TI_IndexConfigAutofocusEnable, &b);

    G_OMX_CORE_SET_CONFIG (gomx, OMX_IndexConfigFocusControl, &config);

    if (config.eFocusControl == OMX_IMAGE_FocusControlAutoLock)
        focusreq_cb.bEnable = OMX_TRUE;
    else
        focusreq_cb.bEnable = OMX_FALSE;

    if (omx_base->gomx->omx_state == OMX_StateExecuting)
    {
        guint32 autofocus_start_time;

        focusreq_cb.nPortIndex = OMX_ALL;
        focusreq_cb.nIndex = OMX_IndexConfigCommonFocusStatus;

        G_OMX_CORE_SET_CONFIG (gomx,
                OMX_IndexConfigCallbackRequest,
                &focusreq_cb);
        GST_DEBUG_OBJECT (self, "AF_cb: enable=%d port=%d",
                focusreq_cb.bEnable, focusreq_cb.nPortIndex);

        if (config.eFocusControl == OMX_IMAGE_FocusControlAutoLock)
        {
            GstStructure *structure = gst_structure_new ("omx_camera",
                    "auto-focus", G_TYPE_BOOLEAN, FALSE, NULL);

            GstMessage *message = gst_message_new_element (
                    GST_OBJECT (self), structure);

            gst_element_post_message (GST_ELEMENT (self), message);

            autofocus_start_time = omap_32k_readraw ();
            GST_CAT_INFO_OBJECT (gstomx_ppm, self,
                    "%d Autofocus started", autofocus_start_time);
        }
    }
}

void
gst_omx_camera_photography_set_autofocus (GstPhotography *photo,
    gboolean autofocus)
{
    OMX_IMAGE_FOCUSCONTROLTYPE enum_focus;

    enum_focus = autofocus ? 3 : 1;
    gst_omx_camera_set_autofocus (photo, enum_focus);
}

void
set_camera_operating_mode (GstOmxCamera *self)
{
    GstOmxBaseSrc *omx_base = GST_OMX_BASE_SRC (self);
    OMX_CONFIG_CAMOPERATINGMODETYPE mode;
    GOmxCore *gomx;

    gomx = (GOmxCore *) omx_base->gomx;
    _G_OMX_INIT_PARAM (&mode);

    switch (self->next_mode)
    {
        case MODE_VIDEO:
            if (self->device == OMX_TI_StereoSensor) {
                mode.eCamOperatingMode = OMX_CaptureStereoImageCapture;
            } else {
                mode.eCamOperatingMode = OMX_CaptureVideo;
            }
            break;
        case MODE_PREVIEW:
            if (self->device == OMX_TI_StereoSensor) {
                mode.eCamOperatingMode = OMX_CaptureStereoImageCapture;
            } else {
                mode.eCamOperatingMode = OMX_CaptureImageProfileBase;
            }
            break;
        case MODE_IMAGE:
        case MODE_VIDEO_IMAGE:
            mode.eCamOperatingMode = OMX_CaptureImageProfileBase;
            break;
        case MODE_IMAGE_HS:
            mode.eCamOperatingMode =
                OMX_CaptureImageHighSpeedTemporalBracketing;
            break;
        default:
            g_assert_not_reached ();
    }
    GST_DEBUG_OBJECT (self, "OMX_CaptureImageMode: set = %d",
            mode.eCamOperatingMode);
    G_OMX_CORE_SET_PARAM (gomx, OMX_IndexCameraOperatingMode, &mode);
}

void
set_property (GObject *obj,
              guint prop_id,
              const GValue *value,
              GParamSpec *pspec)
{
    GstOmxCamera *self = GST_OMX_CAMERA (obj);
    GstOmxBaseSrc *omx_base = GST_OMX_BASE_SRC (self);
    GstPhotography *photo = GST_PHOTOGRAPHY (self);

    if (omx_base->gomx->omx_state < OMX_StateLoaded) {
        GST_ERROR_OBJECT (self, "OMX core not initialized, cannot set property");
        return;
    }

    switch (prop_id)
    {
        case ARG_NUM_IMAGE_OUTPUT_BUFFERS:
        case ARG_NUM_VIDEO_OUTPUT_BUFFERS:
        {
            OMX_PARAM_PORTDEFINITIONTYPE param;
            OMX_U32 nBufferCountActual = g_value_get_uint (value);
            GOmxPort *port = (prop_id == ARG_NUM_IMAGE_OUTPUT_BUFFERS) ?
                    self->img_port : self->vid_port;

            G_OMX_PORT_GET_DEFINITION (port, &param);

            g_return_if_fail (nBufferCountActual >= param.nBufferCountMin);
            param.nBufferCountActual = nBufferCountActual;

            G_OMX_PORT_SET_DEFINITION (port, &param);

            break;
        }
        case ARG_MODE:
        {
            self->next_mode = g_value_get_enum (value);
            GST_DEBUG_OBJECT (self, "mode: %d", self->next_mode);
            break;
        }
        case ARG_SHUTTER:
        {
            self->shutter = g_value_get_enum (value);
            GST_DEBUG_OBJECT (self, "shutter: %d", self->shutter);
            break;
        }
        case ARG_ZOOM:
        {
            gfloat zoom_value;
            zoom_value = g_value_get_float (value);
            gst_omx_camera_photography_set_zoom (photo, zoom_value);
            break;
        }
        case ARG_FOCUS:
        {
            OMX_IMAGE_FOCUSCONTROLTYPE focus_value;

            focus_value = g_value_get_enum (value);
            gst_omx_camera_set_autofocus (photo, focus_value);

            break;
        }
        case ARG_AWB:
        {
            OMX_WHITEBALCONTROLTYPE wb_enum_value;

            wb_enum_value = g_value_get_enum (value);
            gst_omx_camera_set_white_balance_mode (self, wb_enum_value);
            break;
        }
        case ARG_WHITE_BALANCE:
        {
            GstWhiteBalanceMode wb_enum_value;

            wb_enum_value = g_value_get_enum (value);
            gst_omx_camera_photography_set_white_balance_mode (photo,
                                                               wb_enum_value);
            break;
        }
        case ARG_CONTRAST:
        {
            gint contrast = g_value_get_int (value);

            gst_omx_camera_set_contrast (self, contrast);
            break;
        }
        case ARG_BRIGHTNESS:
        {
            gint brightness;

            brightness = g_value_get_int (value);
            gst_omx_camera_set_brightness (self, brightness);
            break;
        }
        case ARG_EXPOSURE:
        {
            OMX_CONFIG_EXPOSURECONTROLTYPE config;
            GOmxCore *gomx;
            OMX_ERRORTYPE error_val = OMX_ErrorNone;

            gomx = (GOmxCore *) omx_base->gomx;
            _G_OMX_INIT_PARAM (&config);
            error_val = OMX_GetConfig (gomx->omx_handle,
                                       OMX_IndexConfigCommonExposure,
                                       &config);
            g_assert (error_val == OMX_ErrorNone);
            config.eExposureControl = g_value_get_enum (value);
            GST_DEBUG_OBJECT (self, "Exposure control = %d",
                              config.eExposureControl);

            G_OMX_CORE_SET_CONFIG (gomx, OMX_IndexConfigCommonExposure, &config);
            break;
        }
        case ARG_ISO:
        {
            OMX_U32 iso_requested;

            iso_requested = g_value_get_uint (value);
            gst_omx_camera_photography_set_iso_speed (photo, iso_requested);
            break;
        }
        case ARG_ROTATION:
        {
            gst_omx_camera_set_rotation (self, g_value_get_uint (value));
            break;
        }
        case ARG_MIRROR:
        {
            gst_omx_camera_set_mirror (self, g_value_get_enum (value));
            break;
        }
        case ARG_SATURATION:
        {
            gint saturation = g_value_get_int (value);

            gst_omx_camera_set_saturation (self, saturation);
            break;
        }
        case ARG_EXPOSUREVALUE:
        {
            gfloat exposure_float_value;
            exposure_float_value = g_value_get_float (value);

            gst_omx_camera_photography_set_ev_compensation (photo,
                exposure_float_value);

            break;
        }
        case ARG_EVCOMPENSATION:
        {
            gfloat exposure_float_value;
            exposure_float_value = g_value_get_float (value);

            gst_omx_camera_photography_set_ev_compensation (photo,
                exposure_float_value);

            break;
        }
        case ARG_MANUALFOCUS:
        {
            gst_omx_camera_set_manual_focus (self, g_value_get_uint (value));
            break;
        }
        case ARG_QFACTORJPEG:
        {
            gst_omx_camera_set_qfactor (self, g_value_get_uint (value));
            break;
        }
        case ARG_DROP_LATE_BUFFERS:
            self->drop_late_buffers = g_value_get_boolean (value);
            break;

        /* Not implemented GstPhotography properties */

        case ARG_APERTURE:
        case ARG_CAPABILITIES:
        case ARG_COLOUR_TONE:
        case ARG_FLASH_MODE:
        case ARG_FOCUS_MODE:
        case ARG_IMAGE_CAPTURE_SUPPORTED_CAPS:
        case ARG_IMAGE_PREVIEW_SUPPORTED_CAPS:
        case ARG_NOISE_REDUCTION:
            break;
#ifdef USE_OMXTICORE
        case ARG_THUMBNAIL_WIDTH:
        {
            gst_omx_camera_set_thumbnail_width (self, g_value_get_int (value));
            break;
        }
        case ARG_THUMBNAIL_HEIGHT:
        {
            gst_omx_camera_set_thumbnail_height (self, g_value_get_int (value));
            break;
        }
        case ARG_FLICKER:
        {
            OMX_COMMONFLICKERCANCELTYPE flicker_enum_value;

            flicker_enum_value = g_value_get_enum (value);
            gst_omx_camera_set_flicker_mode (GST_OMX_CAMERA (photo), flicker_enum_value);
            break;
        }
        case ARG_FLICKER_MODE:
        {
            GstFlickerReductionMode flicker_enum_value;

            flicker_enum_value = g_value_get_enum (value);
            gst_omx_camera_photography_set_flicker_mode (photo,
                                                         flicker_enum_value);
            break;
        }
        case ARG_SCENE:
        {
            OMX_SCENEMODETYPE scene_enum;

            scene_enum = g_value_get_enum (value);
            gst_omx_camera_set_scene_mode (self, scene_enum);
            break;
        }
        case ARG_SCENE_MODE:
        {
            GstSceneMode scene_enum;

            scene_enum = g_value_get_enum (value);
            gst_omx_camera_photography_set_scene_mode (photo, scene_enum);
            break;
        }
        case ARG_VNF:
        {
            gst_omx_camera_set_vnf (self, g_value_get_enum (value));
            break;
        }
        case ARG_YUV_RANGE:
        {
            gst_omx_camera_set_yuv_range (self, g_value_get_enum (value));
            break;
        }
        case ARG_VSTAB:
        {
            OMX_CONFIG_BOOLEANTYPE param;
            OMX_CONFIG_FRAMESTABTYPE config;

            G_OMX_CORE_GET_PARAM (omx_base->gomx, OMX_IndexParamFrameStabilisation, &param);
            G_OMX_CORE_GET_CONFIG (omx_base->gomx, OMX_IndexConfigCommonFrameStabilisation, &config);

            param.bEnabled = config.bStab = g_value_get_boolean (value);
            GST_DEBUG_OBJECT (self, "vstab: param=%d, config=%d", param.bEnabled, config.bStab);

            G_OMX_CORE_SET_PARAM (omx_base->gomx, OMX_IndexParamFrameStabilisation, &param);
            G_OMX_CORE_SET_CONFIG (omx_base->gomx, OMX_IndexConfigCommonFrameStabilisation, &config);

            break;
        }
        case ARG_DEVICE:
        {
            gint device_enum;

            device_enum = g_value_get_enum (value);
            self->device = device_enum;
            gst_omx_camera_set_device (self, device_enum);

            break;
        }
        case ARG_LDC:
        {
            gst_omx_camera_set_ldc (self, g_value_get_boolean (value));
            break;
        }
        case ARG_NSF:
        {
            gst_omx_camera_set_nsf (self, g_value_get_enum (value));
            break;
        }
        case ARG_MTIS:
        {
            gst_omx_camera_set_mtis (self, g_value_get_boolean (value));
            break;
        }
        case ARG_SENSOR_OVERCLOCK:
        {
            gst_omx_camera_set_sensor_overclock (self, g_value_get_boolean (value));
            break;
        }
        case ARG_WB_COLORTEMP:
        {
            gst_omx_camera_set_white_balance_color_temperature (self, g_value_get_uint (value));
            break;
        }
        case ARG_FOCUSSPOT_WEIGHT:
        {
            gst_omx_camera_set_focus_spot_weighting (self, g_value_get_enum (value));
            break;
        }
        case ARG_SHARPNESS:
        {
            gint sharpness = g_value_get_int (value);

            gst_omx_camera_set_sharpness (self, sharpness);
            break;
        }
        case ARG_CAC:
        {
            gst_omx_camera_set_cac (self, g_value_get_boolean (value));
            break;
        }
        case ARG_GBCE:
        {
            gst_omx_camera_set_gbce (self, g_value_get_enum (value));
            break;
        }
        case ARG_GLBCE:
        {
            gst_omx_camera_set_glbce (self, g_value_get_enum (value));
            break;
        }
#endif
        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (obj, prop_id, pspec);
            break;
        }
    }
}

void
get_property (GObject *obj,
              guint prop_id,
              GValue *value,
              GParamSpec *pspec)
{
    GstOmxCamera *self = GST_OMX_CAMERA (obj);
    GstOmxBaseSrc *omx_base = GST_OMX_BASE_SRC (self);
    GstPhotography *photo = GST_PHOTOGRAPHY (self);

    if (omx_base->gomx->omx_state < OMX_StateLoaded) {
        GST_ERROR_OBJECT (self, "OMX core not initialized, cannot get property");
        return;
    }

    switch (prop_id)
    {
        case ARG_NUM_IMAGE_OUTPUT_BUFFERS:
        case ARG_NUM_VIDEO_OUTPUT_BUFFERS:
        {
            OMX_PARAM_PORTDEFINITIONTYPE param;
            GOmxPort *port = (prop_id == ARG_NUM_IMAGE_OUTPUT_BUFFERS) ?
                    self->img_port : self->vid_port;

            G_OMX_PORT_GET_DEFINITION (port, &param);

            g_value_set_uint (value, param.nBufferCountActual);

            break;
        }
        case ARG_MODE:
        {
            GST_DEBUG_OBJECT (self, "mode: %d", self->mode);
            g_value_set_enum (value, self->mode);
            break;
        }
        case ARG_SHUTTER:
        {
            GST_DEBUG_OBJECT (self, "shutter: %d", self->shutter);
            g_value_set_enum (value, self->shutter);
            break;
        }
        case ARG_ZOOM:
        {
            gfloat zoom_value;
            gst_omx_camera_photography_get_zoom (photo, &zoom_value);
            g_value_set_float (value, zoom_value);
            break;
        }
        case ARG_FOCUS:
        {
            OMX_IMAGE_CONFIG_FOCUSCONTROLTYPE config;
            GOmxCore *gomx;
            OMX_ERRORTYPE error_val = OMX_ErrorNone;
            gomx = (GOmxCore *) omx_base->gomx;

            _G_OMX_INIT_PARAM (&config);
            error_val = OMX_GetConfig (gomx->omx_handle,
                                        OMX_IndexConfigFocusControl, &config);
            g_assert (error_val == OMX_ErrorNone);
            config.nPortIndex = omx_base->out_port->port_index;
            GST_DEBUG_OBJECT (self, "AF: param=%d port=%d", config.eFocusControl,
                                                            config.nPortIndex);
            g_value_set_enum (value, config.eFocusControl);

            break;
        }
        case ARG_AWB:
        {
            OMX_WHITEBALCONTROLTYPE wb_enum_value;

            gst_omx_camera_get_white_balance_mode (self, &wb_enum_value);
            g_value_set_enum (value, wb_enum_value);
            break;
        }
        case ARG_WHITE_BALANCE:
        {
            GstWhiteBalanceMode wb_enum_value;

            gst_omx_camera_photography_get_white_balance_mode (photo,
                                                               &wb_enum_value);
            g_value_set_enum (value, wb_enum_value);
            break;
        }
        case ARG_CONTRAST:
        {
            gint contrast;
            gst_omx_camera_get_contrast (self, &contrast);
            g_value_set_int (value, contrast);

            break;
        }
        case ARG_BRIGHTNESS:
        {
            gint brightness;

            gst_omx_camera_get_brightness (self, &brightness);
            g_value_set_int (value, brightness);
            break;
        }
        case ARG_EXPOSURE:
        {
            OMX_CONFIG_EXPOSURECONTROLTYPE config;
            GOmxCore *gomx;
            OMX_ERRORTYPE error_val = OMX_ErrorNone;
            gomx = (GOmxCore *) omx_base->gomx;

            _G_OMX_INIT_PARAM (&config);
            error_val = OMX_GetConfig (gomx->omx_handle,
                                       OMX_IndexConfigCommonExposure,
                                       &config);
            g_assert (error_val == OMX_ErrorNone);
            GST_DEBUG_OBJECT (self, "Exposure control = %d",
                              config.eExposureControl);
            g_value_set_enum (value, config.eExposureControl);

            break;
        }
        case ARG_ISO:
        {
            guint iso_uint_value;

            gst_omx_camera_photography_get_iso_speed (photo, &iso_uint_value);
            g_value_set_uint (value, iso_uint_value);
            break;
        }
        case ARG_ROTATION:
        {
            guint rotation;

            gst_omx_camera_get_rotation (self, &rotation);
            g_value_set_uint (value, rotation);
            break;
        }
        case ARG_MIRROR:
        {
            OMX_MIRRORTYPE mirror;

            gst_omx_camera_get_mirror (self, &mirror);
            g_value_set_enum (value, mirror);
            break;
        }
        case ARG_SATURATION:
        {
            gint saturation;

            gst_omx_camera_get_contrast (self, &saturation);
            g_value_set_int (value, saturation);
            break;
        }
        case ARG_EXPOSUREVALUE:
        {
            gfloat float_value = 0;
            gst_omx_camera_photography_get_ev_compensation (photo, &float_value);
            break;
        }
        case ARG_EVCOMPENSATION:
        {
            gfloat float_value = 0;
            gst_omx_camera_photography_get_ev_compensation (photo, &float_value);
            break;
        }
        case ARG_MANUALFOCUS:
        {
            guint manual_focus;

            gst_omx_camera_get_manual_focus (self, &manual_focus);
            g_value_set_uint (value, manual_focus);
            break;
        }
        case ARG_QFACTORJPEG:
        {
            guint qfactor;

            gst_omx_camera_get_qfactor (self, &qfactor);
            g_value_set_uint (value, qfactor);
            break;
        }
        case ARG_DROP_LATE_BUFFERS:
            g_value_set_boolean (value, self->drop_late_buffers);
            break;

        /* Not implemented GstPhotography properties */

        case ARG_APERTURE:
        case ARG_CAPABILITIES:
        case ARG_COLOUR_TONE:
        case ARG_FLASH_MODE:
        case ARG_FOCUS_MODE:
        case ARG_IMAGE_CAPTURE_SUPPORTED_CAPS:
        case ARG_IMAGE_PREVIEW_SUPPORTED_CAPS:
        case ARG_NOISE_REDUCTION:
            break;
#ifdef USE_OMXTICORE
        case ARG_THUMBNAIL_WIDTH:
        {
            gst_omx_camera_get_thumbnail_width (self, &self->img_thumbnail_width);
            g_value_set_int (value, self->img_thumbnail_width);
            break;
        }
        case ARG_THUMBNAIL_HEIGHT:
        {
            gst_omx_camera_get_thumbnail_height (self, &self->img_thumbnail_height);
            g_value_set_int (value, self->img_thumbnail_height);
            break;
        }
        case ARG_FLICKER:
        {
            OMX_COMMONFLICKERCANCELTYPE flicker_enum_value;

            gst_omx_camera_get_flicker_mode (GST_OMX_CAMERA (photo), &flicker_enum_value);
            g_value_set_enum (value, flicker_enum_value);
            break;
        }
        case ARG_FLICKER_MODE:
        {
            GstFlickerReductionMode flicker_enum_value;

            gst_omx_camera_photography_get_flicker_mode (photo, &flicker_enum_value);
            g_value_set_enum (value, flicker_enum_value);
            break;
        }
        case ARG_SCENE:
        {
            OMX_SCENEMODETYPE scene_enum;

            gst_omx_camera_get_scene_mode (self, &scene_enum);
            g_value_set_enum (value, scene_enum);
            break;
        }
        case ARG_SCENE_MODE:
        {
            GstSceneMode scene_enum;

            gst_omx_camera_photography_get_scene_mode (photo, &scene_enum);
            g_value_set_enum (value, scene_enum);
            break;
        }
        case ARG_VNF:
        {
            OMX_VIDEONOISEFILTERMODETYPE vnf;

            gst_omx_camera_get_vnf (self, &vnf);
            g_value_set_enum (value, vnf);
            break;
        }
        case ARG_YUV_RANGE:
        {
            OMX_VIDEOYUVRANGETYPE range;

            gst_omx_camera_get_yuv_range (self, &range);
            g_value_set_enum (value, range);
            break;
        }
        case ARG_VSTAB:
        {
            OMX_CONFIG_BOOLEANTYPE param;
            OMX_CONFIG_FRAMESTABTYPE config;

            G_OMX_CORE_GET_PARAM (omx_base->gomx, OMX_IndexParamFrameStabilisation, &param);
            G_OMX_CORE_GET_CONFIG (omx_base->gomx, OMX_IndexConfigCommonFrameStabilisation, &config);

            GST_DEBUG_OBJECT (self, "vstab: param=%d, config=%d", param.bEnabled, config.bStab);
            g_value_set_boolean (value, param.bEnabled && config.bStab);

            break;
        }
        case ARG_DEVICE:
        {
            OMX_CONFIG_SENSORSELECTTYPE config;
            GOmxCore *gomx;
            OMX_ERRORTYPE error_val = OMX_ErrorNone;

            g_value_set_enum (value, self->device);
            gomx = (GOmxCore *) omx_base->gomx;
            _G_OMX_INIT_PARAM (&config);
            error_val = OMX_GetConfig (gomx->omx_handle,
                                       OMX_TI_IndexConfigSensorSelect, &config);
            g_assert (error_val == OMX_ErrorNone);
            GST_DEBUG_OBJECT (self, "Device src=%d", config.eSensor);

            break;
        }
        case ARG_LDC:
        {
            gboolean ldc;

            gst_omx_camera_get_ldc (self, &ldc);
            g_value_set_boolean (value, ldc);
            break;
        }
        case ARG_NSF:
        {
            OMX_VIDEONOISEFILTERMODETYPE nsf;

            gst_omx_camera_get_nsf (self, &nsf);
            g_value_set_enum (value, nsf);
            break;
        }
        case ARG_MTIS:
        {
            gboolean mtis;

            gst_omx_camera_get_mtis (self, &mtis);
            g_value_set_boolean (value, mtis);
            break;
        }
        case ARG_SENSOR_OVERCLOCK:
        {
            gboolean sensor_overclock;

            gst_omx_camera_get_sensor_overclock (self, &sensor_overclock);
            g_value_set_boolean (value, sensor_overclock);
            break;
        }
        case ARG_WB_COLORTEMP:
        {
            guint wbct;

            gst_omx_camera_get_white_balance_color_temperature (self, &wbct);
            g_value_set_uint (value, wbct);
            break;
        }
        case ARG_FOCUSSPOT_WEIGHT:
        {
            OMX_TI_CONFIG_FOCUSSPOTMODETYPE focus;

            gst_omx_camera_get_focus_spot_weighting (self, &focus);
            g_value_set_enum (value, focus);
            break;
        }
        case ARG_SHARPNESS:
        {
            gint sharpness;

            gst_omx_camera_get_sharpness (self, &sharpness);
            g_value_set_int (value, sharpness);
            break;
        }
        case ARG_CAC:
        {
            gboolean cac;

            gst_omx_camera_get_cac (self, &cac);
            g_value_set_boolean (value, cac);
            break;
        }
        case ARG_GBCE:
        {
            OMX_TI_BRIGHTNESSCONTRASTCRTLTYPE gbce;

            gst_omx_camera_get_gbce (self, &gbce);
            g_value_set_enum (value, gbce);
            break;
        }
        case ARG_GLBCE:
        {
            OMX_TI_BRIGHTNESSCONTRASTCRTLTYPE gbce;

            gst_omx_camera_get_glbce (self, &gbce);
            g_value_set_enum (value, gbce);
            break;
        }
#endif
        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (obj, prop_id, pspec);
            break;
        }
    }
}


void
install_camera_properties(GObjectClass *gobject_class)
{
    g_object_class_install_property (gobject_class, ARG_NUM_IMAGE_OUTPUT_BUFFERS,
            g_param_spec_uint ("image-output-buffers", "Image port output buffers",
                    "The number of OMX image port output buffers",
                    1, 10, 4, G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_NUM_VIDEO_OUTPUT_BUFFERS,
            g_param_spec_uint ("video-output-buffers", "Video port output buffers",
                    "The number of OMX video port output buffers",
                    1, 10, 4, G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_MODE,
            g_param_spec_enum ("mode", "Camera Mode",
                    "image capture, video capture, or both",
                    GST_TYPE_OMX_CAMERA_MODE,
                    MODE_PREVIEW,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_SHUTTER,
            g_param_spec_enum ("shutter", "Shutter State",
                    "shutter button state",
                    GST_TYPE_OMX_CAMERA_SHUTTER,
                    SHUTTER_OFF,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_ZOOM,
            g_param_spec_float ("zoom", "Digital Zoom",
                    "digital zoom factor/level",
                    MIN_ZOOM_LEVEL, MAX_ZOOM_LEVEL, DEFAULT_ZOOM_LEVEL,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_FOCUS,
            g_param_spec_enum ("focus", "Auto Focus",
                    "auto focus state",
                    GST_TYPE_OMX_CAMERA_FOCUS,
                    DEFAULT_FOCUS,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_AWB,
            g_param_spec_enum ("awb", "Auto White Balance",
                    "auto white balance state",
                    GST_TYPE_OMX_CAMERA_AWB,
                    DEFAULT_AWB,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_CONTRAST,
            g_param_spec_int ("contrast", "Contrast",
                    "contrast level", MIN_CONTRAST_LEVEL,
                    MAX_CONTRAST_LEVEL, DEFAULT_CONTRAST_LEVEL,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_BRIGHTNESS,
            g_param_spec_int ("brightness", "Brightness",
                    "brightness level", MIN_BRIGHTNESS_LEVEL,
                    MAX_BRIGHTNESS_LEVEL, DEFAULT_BRIGHTNESS_LEVEL,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_EXPOSURE,
            g_param_spec_enum ("exposure", "Exposure Control",
                    "exposure control mode",
                    GST_TYPE_OMX_CAMERA_EXPOSURE,
                    DEFAULT_EXPOSURE,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_ISO,
            g_param_spec_uint ("iso-speed", "ISO Speed",
                    "ISO speed level", MIN_ISO_LEVEL,
                    MAX_ISO_LEVEL, DEFAULT_ISO_LEVEL,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_ROTATION,
            g_param_spec_uint ("rotation", "Rotation",
                    "Image rotation",
                    0, 270, DEFAULT_ROTATION , G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_MIRROR,
            g_param_spec_enum ("mirror", "Mirror",
                    "Mirror image",
                    GST_TYPE_OMX_CAMERA_MIRROR,
                    DEFAULT_MIRROR,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_SATURATION,
            g_param_spec_int ("saturation", "Saturation",
                    "Saturation level", MIN_SATURATION_VALUE,
                    MAX_SATURATION_VALUE, DEFAULT_SATURATION_VALUE,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_EXPOSUREVALUE,
            g_param_spec_float ("exposure-value", "Exposure value",
                    "EVCompensation level", MIN_EXPOSURE_VALUE,
                    MAX_EXPOSURE_VALUE, DEFAULT_EXPOSURE_VALUE,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_MANUALFOCUS,
            g_param_spec_uint ("manual-focus", "Manual Focus",
                    "Manual focus level, 0:Infinity  100:Macro",
                    MIN_MANUALFOCUS, MAX_MANUALFOCUS, DEFAULT_MANUALFOCUS,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_QFACTORJPEG,
            g_param_spec_uint ("qfactor", "Q Factor JPEG",
                    "JPEG Q Factor level, 1:Highest compression  100:Best quality",
                    MIN_QFACTORJPEG, MAX_QFACTORJPEG, DEFAULT_QFACTORJPEG,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_DROP_LATE_BUFFERS,
            g_param_spec_boolean ("drop-late-buffers", "Drop late buffers",
                    "Whether to drop old buffers when more than one is ready",
                    DEFAULT_DROP_LATE_BUFFERS, G_PARAM_READWRITE));

    /* GstPhotography properties*/

    g_object_class_install_property (gobject_class, ARG_WHITE_BALANCE,
            g_param_spec_enum ("white-balance-mode",
                    "GstPhotography White Balance",
                    "Auto white balance state as defined in GstPhotography",
                    GST_TYPE_WHITE_BALANCE_MODE,
                    GST_PHOTOGRAPHY_WB_MODE_AUTO,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_EVCOMPENSATION,
            g_param_spec_float ("ev-compensation", "Exposure compensation",
                    "Exposure Value Compensation level",
                    -2.5, 2.5, DEFAULT_EXPOSURE_VALUE,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_FLICKER_MODE,
            g_param_spec_enum ("flicker-mode", "Photography Flicker Control",
                    "flicker control state as described in GstPhotography",
                    GST_TYPE_FLICKER_REDUCTION_MODE,
                    GST_PHOTOGRAPHY_FLICKER_REDUCTION_OFF,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_SCENE_MODE,
            g_param_spec_enum ("scene-mode", "GstPhotography Scene Mode",
                    "Scene mode as in GstPhotography",
                    GST_TYPE_SCENE_MODE,
                    GST_PHOTOGRAPHY_SCENE_MODE_AUTO,
                    G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
    g_object_class_install_property (gobject_class, ARG_APERTURE,
            g_param_spec_uint ("aperture", "GstPhotography Aperture",
                    "Aperture as in GstPhotography",
                    0, 255, 0,
                    G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
    g_object_class_install_property (gobject_class, ARG_CAPABILITIES,
            g_param_spec_ulong ("capabilities",
                    "GstPhotography capabilities property",
                    "Capabilities as in GstPhotography",
                    0, G_MAXULONG, 0,
                    G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
    g_object_class_install_property (gobject_class, ARG_COLOUR_TONE,
            g_param_spec_enum ("colour-tone-mode",
                    "GstPhotography colour tone mode",
                    "Mode of colour tone as in GstPhotography",
                    GST_TYPE_COLOUR_TONE_MODE,
                    GST_PHOTOGRAPHY_COLOUR_TONE_MODE_NORMAL,
                    G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
    g_object_class_install_property (gobject_class, ARG_FLASH_MODE,
            g_param_spec_enum ("flash-mode",
                    "GstPhotography flash mode",
                    "Flash mode as in GstPhotography",
                    GST_TYPE_FLASH_MODE,
                    GST_PHOTOGRAPHY_FLASH_MODE_AUTO,
                    G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
    g_object_class_install_property (gobject_class, ARG_FOCUS_MODE,
            g_param_spec_enum ("focus-mode",
                    "GstPhotography focus mode",
                    "Focus mode as in GstPhotography",
                    GST_TYPE_FOCUS_MODE,
                    GST_PHOTOGRAPHY_FOCUS_MODE_AUTO,
                    G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
    g_object_class_install_property (gobject_class,
            ARG_IMAGE_CAPTURE_SUPPORTED_CAPS,
            g_param_spec_boxed ("image-capture-supported-caps",
                    "GstPhotography image capture supported caps",
                    "Caps describing supported image capture formats",
                    GST_TYPE_CAPS,
                    G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
    g_object_class_install_property (gobject_class,
            ARG_IMAGE_PREVIEW_SUPPORTED_CAPS,
            g_param_spec_boxed ("image-preview-supported-caps",
                    "GstPhotography image preview supported caps",
                    "Caps describing supported image preview formats",
                    GST_TYPE_CAPS,
                    G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
    g_object_class_install_property (gobject_class, ARG_NOISE_REDUCTION,
            g_param_spec_flags ("noise-reduction",
                    "GstPhotography noise reduction",
                    "Which noise reduction modes are enabled (0 = disabled)",
                    GST_TYPE_PHOTOGRAPHY_NOISE_REDUCTION,
                    0, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

#ifdef USE_OMXTICORE
    g_object_class_install_property (gobject_class, ARG_THUMBNAIL_WIDTH,
            g_param_spec_int ("thumb-width", "Thumbnail width",
                    "Thumbnail width in pixels", MIN_THUMBNAIL_LEVEL,
                    MAX_THUMBNAIL_LEVEL, DEFAULT_THUMBNAIL_WIDTH,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_THUMBNAIL_HEIGHT,
            g_param_spec_int ("thumb-height", "Thumbnail height",
                    "Thumbnail height in pixels", MIN_THUMBNAIL_LEVEL,
                    MAX_THUMBNAIL_LEVEL, DEFAULT_THUMBNAIL_HEIGHT,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_FLICKER,
            g_param_spec_enum ("flicker", "Flicker Control",
                    "flicker control state",
                    GST_TYPE_OMX_CAMERA_FLICKER,
                    DEFAULT_FLICKER,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_SCENE,
            g_param_spec_enum ("scene", "Scene Mode",
                    "Scene mode",
                    GST_TYPE_OMX_CAMERA_SCENE,
                    DEFAULT_SCENE,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_VNF,
            g_param_spec_enum ("vnf", "Video Noise Filter",
                    "is video noise filter algorithm enabled?",
                    GST_TYPE_OMX_CAMERA_VNF,
                    DEFAULT_VNF,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_YUV_RANGE,
            g_param_spec_enum ("yuv-range", "YUV Range",
                    "YUV Range",
                    GST_TYPE_OMX_CAMERA_YUV_RANGE,
                    DEFAULT_YUV_RANGE,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_VSTAB,
            g_param_spec_boolean ("vstab", "Video Frame Stabilization",
                    "is video stabilization algorithm enabled?",
                    TRUE,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_DEVICE,
            g_param_spec_enum ("device", "Camera sensor",
                    "Image and video stream source",
                    GST_TYPE_OMX_CAMERA_DEVICE,
                    DEFAULT_DEVICE,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_LDC,
            g_param_spec_boolean ("ldc", "Lens Distortion Correction",
                    "Lens Distortion Correction state",
                    FALSE,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_NSF,
            g_param_spec_enum ("nsf", "ISO noise suppression filter",
                    "low light environment noise filter",
                    GST_TYPE_OMX_CAMERA_NSF,
                    DEFAULT_NSF,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_MTIS,
            g_param_spec_boolean ("mtis", "Motion triggered image stabilisation mode",
                    "Motion triggered image stabilisation mode",
                    FALSE,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_SENSOR_OVERCLOCK,
            g_param_spec_boolean ("overclock", "Sensor over-clock mode",
                    "Sensor over-clock mode",
                    FALSE,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_WB_COLORTEMP,
            g_param_spec_uint ("wb-colortemp",
                    "White Balance Color Temperature",
                    "White balance color temperature", MIN_WB_COLORTEMP_VALUE,
                    MAX_WB_COLORTEMP_VALUE, DEFAULT_WB_COLORTEMP_VALUE,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_FOCUSSPOT_WEIGHT,
            g_param_spec_enum ("focusweight", "Focus Spot Weight mode",
                    "Focus spot weight mode",
                    GST_TYPE_OMX_CAMERA_FOCUSSPOT_WEIGHT,
                    DEFAULT_FOCUSSPOT_WEIGHT,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_SHARPNESS,
            g_param_spec_int ("sharpness", "Sharpness value",
                    "Sharpness value, 0:automatic mode)", MIN_SHARPNESS_VALUE,
                    MAX_SHARPNESS_VALUE, DEFAULT_SHARPNESS_VALUE,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_CAC,
            g_param_spec_boolean ("cac", "Chromatic Aberration Correction",
                    "Chromatic Aberration Correction state",
                    FALSE,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_GBCE,
            g_param_spec_enum ("gbce", "Global Brightness Contrast Enhance",
                    "global brightness contrast enhance",
                    GST_TYPE_OMX_CAMERA_BCE,
                    DEFAULT_GBCE,
                    G_PARAM_READWRITE));
    g_object_class_install_property (gobject_class, ARG_GLBCE,
            g_param_spec_enum ("lbce", "Local Brightness Contrast Enhance",
                    "local brightness contrast enhance",
                    GST_TYPE_OMX_CAMERA_BCE,
                    DEFAULT_GLBCE,
                    G_PARAM_READWRITE));
#endif

}

void
gst_omx_camera_setup_stereo (GstOmxCamera * self)
{
    GstOmxBaseSrc *omx_base;
    GOmxPort *port;
    OMX_TI_FRAMELAYOUTTYPE frameLayout;
    OMX_TI_CONFIG_CONVERGENCETYPE acParams;

    omx_base = GST_OMX_BASE_SRC (self);
    port = omx_base->out_port;

    _G_OMX_INIT_PARAM(&frameLayout);
    frameLayout.nPortIndex = port->port_index;
    frameLayout.eFrameLayout = OMX_TI_StereoFrameLayoutLeftRight;
    frameLayout.nSubsampleRatio = 1 << 7; /* in Q15.7 format */
    G_OMX_CORE_SET_PARAM (omx_base->gomx,
        OMX_TI_IndexParamStereoFrmLayout, &frameLayout);

    _G_OMX_INIT_PARAM(&acParams);
    acParams.nPortIndex = port->port_index;
    acParams.nManualConverence = 0;
    acParams.eACMode = OMX_TI_AutoConvergenceModeDisable;
    G_OMX_CORE_SET_CONFIG (omx_base->gomx,
        OMX_TI_IndexConfigAutoConvergence, &acParams);
}





