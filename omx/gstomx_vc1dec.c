/*
 * Copyright (C) 2011 Texas Instruments.
 *
 * Authors: Alessandro Decina <alessandro.decina@collabora.co.uk>
 *          Juan Yanez <sandino@ti.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */


#include "gstomx_vc1dec.h"
#include "gstomx.h"

GSTOMX_BOILERPLATE (GstOmxVc1Dec, gst_omx_vc1dec, GstOmxBaseVideoDec, GST_OMX_BASE_VIDEODEC_TYPE);

static GstCaps *
generate_sink_template (void)
{
    GstCaps *caps;

    caps = gst_caps_from_string ("video/x-wmv, "
        "wmvversion = (int)[ 2, 3 ], "
        "format = (fourcc){ WVC1, WMV3, WMV2, WMV1 }, "
        "width = (int)[ 16, 2048 ], "
        "height = (int)[ 16, 2048 ], "
        "framerate = (fraction)[ 0, max ];");

    return caps;
}

static gboolean
sink_setcaps (GstPad * pad, GstCaps * caps)
{
  guint32 format;
  gboolean ret = FALSE;
  GstStructure *s = gst_caps_get_structure (caps, 0);

  GstOmxVc1Dec *self = GST_OMX_VC1DEC (gst_pad_get_parent (pad));

  if (!(gst_structure_get_int (s, "width", &self->width) &&
          gst_structure_get_int (s, "height", &self->height)))
      goto out;

  ret = gst_structure_get_fourcc (s, "format", &format);
  if (ret) {
    switch (format) {
      case GST_MAKE_FOURCC ('W', 'V', 'C', '1'):
        self->level = 4;
        break;
      case GST_MAKE_FOURCC ('W', 'M', 'V', '3'):
        self->level = 3;
        break;
      case GST_MAKE_FOURCC ('W', 'M', 'V', '2'):
        self->level = 2;
        break;
      case GST_MAKE_FOURCC ('W', 'M', 'V', '1'):
        self->level = 1;
        break;
      default:
        ret = FALSE;
        break;
    }
  }

  GST_INFO_OBJECT (self, "level %d, %dx%d", self->level, self->width, self->height);

out:
  gst_object_unref (self);
  return ret;
}

static void
inject_codec_data (GstOmxBaseFilter * omx_base)
{
  GstOmxVc1Dec *self = GST_OMX_VC1DEC (omx_base);
  GstBuffer *codec_data = omx_base->codec_data;
  GstBuffer *buf = NULL;

  if (self->level == 4) {
    /* for VC-1 Advanced Profile, strip off first byte, and
     * send rest of codec_data unmodified;
     */
    buf = gst_buffer_create_sub (codec_data, 1,
        GST_BUFFER_SIZE (codec_data) - 1);
  } else {
    guint8 *data;
    guint32 val;

    buf = gst_buffer_new_and_alloc (8 + 4 + 24);
    data = GST_BUFFER_DATA (buf);

    val = 0xc5ffffff;         /* we don't know the number of frames */
    GST_WRITE_UINT32_LE (data, val);
    data += 4;

    val = 0x00000004;
    GST_WRITE_UINT32_LE (data, val);
    data += 4;

    val = GST_READ_UINT32_LE (GST_BUFFER_DATA (codec_data));
    val |= 0x01 << 24;
    GST_WRITE_UINT32_LE (data, val);
    data += 4;
  
    GST_WRITE_UINT32_LE (data, self->height);
    data += 4;
    GST_WRITE_UINT32_LE (data, self->width);
    data += 4;
    GST_INFO_OBJECT (self, "seq hdr resolution: %dx%d",
        self->width, self->height); 

    val = 0x0000000c;
    GST_WRITE_UINT32_LE (data, val);
    data += 4;
    
    val = 0x00000000;         /* not sure how to populate, but codec ignores anyways */ 
    GST_WRITE_UINT32_BE (data, val);
    data += 4;
    GST_WRITE_UINT32_BE (data, val);
    data += 4;
    GST_WRITE_UINT32_BE (data, val);
    data += 4;

    GST_INFO_OBJECT (self, "codec_data bufsize %d", GST_BUFFER_SIZE (buf));
  }

  GST_BUFFER_FLAG_SET (buf, GST_BUFFER_FLAG_IN_CAPS);
  g_omx_port_send (omx_base->in_port, buf);
}

static void
type_base_init (gpointer g_class)
{
    GstElementClass *element_class;

    element_class = GST_ELEMENT_CLASS (g_class);

    {
        GstElementDetails details;

        details.longname = "OpenMAX IL VC1 video decoder";
        details.klass = "Codec/Decoder/Video";
        details.description = "Decodes video in VC1 format with OpenMAX IL";
        details.author = "Alessandro Decina <alessandro.decina@collabora.co.uk>";

        gst_element_class_set_details (element_class, &details);
    }

    {
        GstPadTemplate *template;

        template = gst_pad_template_new ("sink", GST_PAD_SINK,
                                         GST_PAD_ALWAYS,
                                         generate_sink_template ());

        gst_element_class_add_pad_template (element_class, template);
    }
}

static GstFlowReturn
pad_chain (GstPad *pad,
           GstBuffer *buf)
{
    GstOmxBaseFilter *basefilter;
    GstOmxVc1Dec *self;
    GOmxCore *gomx;

    self = GST_OMX_VC1DEC (GST_OBJECT_PARENT (pad));
    basefilter = GST_OMX_BASE_FILTER (self);
    gomx = basefilter->gomx;

    if (self->level == 4) {
        static guint8 sc[] = { 0x00, 0x00, 0x01, 0x0d };
        GstBuffer *buf1;

        /* prepend start codes... we really need a parser for this */
        buf1 = gst_buffer_new_and_alloc (4 + GST_BUFFER_SIZE (buf));
        memcpy (GST_BUFFER_DATA (buf1), &sc, 4);
        memcpy (GST_BUFFER_DATA (buf1) + 4, GST_BUFFER_DATA  (buf), GST_BUFFER_SIZE (buf));

        gst_buffer_unref (buf);
        buf = buf1;
    }

  return GST_OMX_BASE_FILTER_CLASS (parent_class)->pad_chain (pad, buf);
}


static void
type_class_init (gpointer g_class,
                 gpointer class_data)
{
    GstOmxBaseFilterClass *bclass;

    bclass = GST_OMX_BASE_FILTER_CLASS (g_class);
    bclass->pad_chain = pad_chain;
}

static void
type_instance_init (GTypeInstance *instance,
                    gpointer g_class)
{
    GstOmxBaseFilter *base_filter;
    GstOmxBaseVideoDec *omx_base;
    GstOmxVc1Dec *self;


    base_filter = GST_OMX_BASE_FILTER (instance);
    omx_base = GST_OMX_BASE_VIDEODEC (instance);
    self = GST_OMX_VC1DEC (instance);

    omx_base->sink_setcaps = sink_setcaps;
    base_filter->inject_codec_data = inject_codec_data;
    omx_base->compression_format = OMX_VIDEO_CodingWMV;

    self->level = 0;
}
